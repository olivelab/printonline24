<div class="small-banners">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <a href="/products/elektronnye-izdaniya/" class="clearfix">
          <img src="<?=MEDIA_PATH;?>images/index-banner-small.png" alt="" class="left">
          <div class="overflow">Электронные издания и публикаци<span>от <b>5000</b> руб.</span></div>
        </a>
      </div>
      <div class="col-md-6">
        <a href="/shipping-and-payment/payment-methods/" class="clearfix">
          <img src="<?=MEDIA_PATH;?>images/index-banner-small2.png" alt="" class="right" style="padding:23px 14px 0 0">
          <div class="overflow" style="padding:30px 6px 0 28px">Безопасная оплата онлайн<p>Все платежи проходят через защищенное безопасное соединение</p></div>
        </a>
      </div>
    </div>
  </div>
<!--/small banners-->
</div>