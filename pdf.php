<?
define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/dompdf/autoload.inc.php");



use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->set_base_path('/');
$dompdf->set_option('defaultFont', '');
$dompdf->set_option('isRemoteEnabled', TRUE);
//$dompdf->set_option('isHtml5ParserEnabled', true);
$dompdf->set_option('chroot', '/');

ob_start();
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Счет на заказ | Originals</title>
        <style type="text/css">
            body {font-family:DejaVu Sans;}
            .h1bask {
                font:16px helveticaneuecyr-bold !important;
                color: #0f5d81;
                margin:0 0 10px;
            }
            font.h1bask {
                font:14px helveticaneuecyr-light !important;
                display:block;
            }
            .order_contact_text {
                color:#8c0412;
                font:16px helveticaneuecyr-roman !important;
            }
            .prod_konf_head_text_order {
                font:16px helveticaneuecyr-bold !important;
                color:#000;
            }
            .prod_konf_head_text_order.order_text_18,
            .prod_konf_head_text_order.order_contact_color_text {
                font-size:16px !important;
                color:#8c0412
            }
            .clear {
                clear:both;
                display:block;
             }
            .left {float:left; display:block;}
            .right {float:right; display:block;}
            .table-simple {
                    font:10px/16px  helveticaneuecyr-light;
            }
                .table-simple > tbody > tr > td {
                    padding:10px 20px !important;
                    border:1px solid #719fb7 !important;
                    border-collapse:collapse;
                }
    
        </style>
    </head>
    <body>
        <div class="holder" style="width:100%; border: 0px solid blue; padding:0px;">
            <div class="prod_s_head_div" style="border:0px solid red; margin:0px;width:100%;">
		    
                <table border="0" style="width: 100%;">
                    <tr>
                        <td align="left" valign="bottom">
                            <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/logo.gif" border="0" height="60">
                        </td>
                        <td align="right" valign="top">
                        <div style="width:100%;text-align:right;" class="order_contact_text">+7 495 105 95 38
<br>info@print-online24.ru</div>
                        
                        
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                        <td></td>
                    </tr>
		    
                    <tr>		    
                        <td align="center" valign="bottom" width="70%" colspan="2">
                            <h1 class="h1bask">Коммерческое предложение от компании Полиграфия Онлайн</h1>
                            <font class="h1bask">действительно на <?=date("d.m.Y");?></font>
                        </td>
                    </tr>
                    <tr><td valign="bottom" align="justify" colspan="2" style="padding-top:10px;">
		    <a class="prod_konf_head_text_order order_contact_text order_text_18 no_underline" href="mailto:siteshop@print-online24.ru" style="text-decoration:none;"><b>Внимание!
		    </b></a><br> 
		    <a class="prod_konf_head_text_order order_contact_text order_text_16 order_text_black" href="mailto:siteshop@print-online24.ru" style="text-decoration:none;"><b>Для своевременной отгрузки необходимо заполнить реквизиты Вашей компании в личном кабинете или выслать карточку организации ответным письмом.</b></a>
		    
		    
		    </td></tr>
                </table>
            </div>
            
        <div class="order_basic" style="padding:0px; margin-top:0px;margin-bottom:0px; width:100%; margin-top:0px;">
            <div class="p_ch" style="margin-top: 0px; width:100%;padding-bottom:10px;">
	            <br><br>
                <font class="prod_konf_head_text_order order_contact_color_text">
                    <b>Печатные листы</b>
                </font>
                <br><br>
                <table class="order_basic-list table-simple" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="500px">
                            <div>
                                <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="29%">Формат</td>
                                        <td width="49%">64х45 см.</td>
                                    </tr>                            
                                    <tr>
                                        <td width="29%">Красочность</td>
                                        <td width="49%">1+0 Kontur</td>
                                    </tr>                            
                                    <tr>
                                        <td width="29%">Бумага</td>
                                        <td width="49%">80 гр/м мелованная</td>
                                    </tr>
                                    <tr>
                                        <td width="29%">Тираж</td>
                                        <td width="49%">100</td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                       </td>
                       <td valign="bottom">
                            <div>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                        <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                        </td>
                                        <td align="right" width="70" nowrap style='white-space:nowrap;'>1 692,86</td>
                                    </tr>
                                </table>
                            </div>
                       </td>
                   </tr>   
                   
                   <tr>
                    <td>
                        <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="29%">Качество бумаги </td>
                                <td width="49%">глянцевая мелованная </td>
                            </tr>                            
                            <tr>
                                <td width="29%">Отделка </td>
                                <td width="49%">не выбрано </td>
                            </tr>                            
                            <tr>
                                <td width="29%">Проверка макета </td>
                                <td width="49%">не нужна</td>
                            </tr>
                            <tr>
                                <td width="29%">Доставка </td>
                                <td width="49%">самовывоз из г.Железнодорожный </td>
                            </tr>
                        </table>
                    </td>    
                    <td valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right">
                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                </td>
                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right">
                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                </td>
                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right">
                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                </td>
                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right">
                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                </td>
                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                            </tr>
                        </table>
                    </td>                
                   </tr>
                    <tr>
                        <td colspan="2">    
                            <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="29%">Стоимость продукции (Включая НДС) </td>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                                </td>
                                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                            
                                <!--<tr>
                                    <td width="29%">В том числе 18% НДС </td>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                                </td>
                                                <td align="right" width="70" nowrap style='white-space:nowrap;'>0,00</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>-->
                            </table>
                        </td>
                    </tr>                 
                </table>
                <style>
                .total-order {
                    background:#126086;
                    color:#fff;
                }
                    .total-order td {
                        padding:0 20px 10px;
                        vertical-align:top;
                        font-size:12px;
                    }
                        .total-order td h2 {
                            font-size:16px;
                            margin:0;
                            padding:0;
                        }
                        .total-order td small {
                            font-size:12px;
                        }
                        .total-order td div {
                            font:18px helveticaneuecyr-bold !important;
                        }
                </style>
                <table class="total-order" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top"><h2>Общая сумма:</h2></td>
                        <td align="right">
                            <div>1 692 руб. 86 коп.</div>
                            <small><?=FormatHelper::num2str(1692.86);?></small>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>В том числе 18% НДС:</td>
                        <td align="right">
                            <div>258 руб. 23 коп.</div>
                            <small><?=FormatHelper::num2str(258.23);?></small>
                        </td>
                    </tr>-->
                    <tr>
                        <td colspan="2" align="right"><br><div>Общий вес: 2,30 кг.</div></td>
                    </tr>
                </table>
            </div>             
        </div>
    </body>
</html>
<?
$html = ob_get_contents();
ob_end_clean();
$dompdf->loadHtml($html);
$dompdf->setPaper('A4');
$dompdf->render();
$dompdf->stream('test.pdf',array('Attachment'=>0));


/*$mpdf->WriteHTML($html); */
/*$mpdf=new mPDF('c'); 
$mpdf->charset_in = 'cp1251';
$mpdf->SetDisplayMode("fullpage");
$mpdf->autoLangToFont = true;
$mpdf->WriteHTML($html);
$mpdf->Output();*/



//$mpdf->WriteHTML($html, 2); /*формируем pdf*/
//$mpdf->Output('mpdf.pdf', 'I');
//exit();
?>
