<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><b>Наш телефон в Москве:</b> +7 495 105 9538 <br>
<b>E-mail:</b> <a href="mailto:info@print-online24.ru">info@print-online24.ru</a> <br>
<br>
Мы на связи по телефону c понедельника по пятницу с 10:00 до 18:00 кроме официальных праздников.<br>
 Вы можете самостоятельно посчитать, оформить, <a href="http://print-online24.ru/shipping-and-payment/payment-methods/index.php">оплатить</a> и выбрать дату доставки Вашего тиража.<br>
 <br>
 <b>Адрес нашего офиса:</b><br>
 115551, город Москва, улица Шипиловский проезд, дом 47, офис 1405<br>
 <br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"SCALELINE",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.609462707965896;s:10:\"yandex_lon\";d:37.698765813491995;s:12:\"yandex_scale\";i:17;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.698653160713;s:3:\"LAT\";d:55.609439933814;s:4:\"TEXT\";s:0:\"\";}}}",
		"MAP_HEIGHT" => "500",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_DRAGGING",)
	)
);?><br>
 <br>
 &nbsp;&nbsp;<br>
 <b>Адрес нашего производства:</b><br>
 143982, Московская область, город Железнодорожный, улица Гидрогородок, дом 15 <br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"SCALELINE",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.75396234533799;s:10:\"yandex_lon\";d:37.961674556875494;s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.961588726187;s:3:\"LAT\";d:55.753847385397;s:4:\"TEXT\";s:46:\"Полиграфия Принт-Онлайн24\";}}}",
		"MAP_HEIGHT" => "500",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_DRAGGING",)
	)
);?><br>
<br>
<div style="width:50%">
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"",
	Array(
		"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		),
		"CACHE_TYPE" => "N",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"LIST_URL" => "",	// Страница со списком результатов
		"EDIT_URL" => "",	// Страница редактирования результата
		"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"WEB_FORM_ID" => "1",
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>