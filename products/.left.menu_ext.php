<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 
global $APPLICATION;
if(CModule::IncludeModule("iblock")) {
$aMenuLinksExt=$APPLICATION->IncludeComponent("dev:menu.sections", "", array(
	"IS_SEF" => "Y",
	"SEF_BASE_URL" => "/products/",
	"SECTION_PAGE_URL" => "#SECTION_CODE#/",
	"DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#",
	"IBLOCK_TYPE" => "products",
	"IBLOCK_ID" => "1",
	"DEPTH_LEVEL" => "4",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000"
	),
	false
);
/*	$aMenuLinksExt = array();
  $arFilter = array('IBLOCK_ID' => 1, "ACTIVE"=>'Y', 'DEPTH_LEVEL'=>1); 
  $rsSect = CIBlockSection::GetList(array('sort' => 'asc'),$arFilter, false, array('NAME','SECTION_PAGE_URL','PICTURE','UF_*'));
  while ($arSect = $rsSect->GetNext())
  {
    $aMenuLinksExt[] = Array(
      $arSect['NAME'],
      $arSect['SECTION_PAGE_URL'],
      Array(),
      Array(
       'IMG'=>CFile::GetPath($arSect['PICTURE']),
       'BG'=>$arSect['UF_BG']
      ),
      ""
    );
  }*/
/*  $arOrder = Array("SORT"=>"ASC");
  $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL");
  $arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
  $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
  while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();            
    $aMenuLinksExt[] = Array(
      $arFields["NAME"],
      $arFields["~DETAIL_PAGE_URL"],
      array(),
      array(
         "FROM_IBLOCK" => true,
         "IS_PARENT" => false,
         "DEPTH_LEVEL" => 4,
      ),
     );
  }*/
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);