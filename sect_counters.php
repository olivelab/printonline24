<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K5R5P5');</script>
<!-- End Google Tag Manager -->

<meta name="yandex-verification" content="5024606858a6e891" />


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter40158155 = new Ya.Metrika({
                    id:40158155,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/40158155" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- widget --><link rel="stylesheet" href="upload/css/widget.css" /><script>if(typeof $ == "undefined"){ document.write("<script type='text/javascript' src='https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/styles/chats/modernnew/js/vendor/jquery-1.11.2.min.js'></"+"script>"); }</script><script src="https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/styles/chats/modernnew/js/vendor/jquery.inputmask.js"></script><script src="https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/styles/chats/modernnew/js/vendor/common.js"></script><div><div id="mibew-agent-button" class="mcntelecom-from-site-call mcntelecom-active"> <div onclick="clickWidget('57fce60d3df5957', 'call');return false;" class="mcntelecom-phone"></div> <div onclick="clickWidget('57fce60d3df5957', 'chat');return false;" class="mcntelecom-chat"> <div data-background="https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/styles/chats/modernnew/images/sprite.png" class="mcntelecom-bg-animation"></div> </div> </div> <div class="mcntelecom-feedback"> <div class="mcntelecom-exit"></div> </div></div><script type="text/javascript" src="https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/js/compiled/chat_popup.js"></script><script type="text/javascript">Mibew.ChatPopup.init({"id":"57fce60d3df5957","url":"https:\/\/feedback.mcn.ru\/feedback\/chat-57dbd123ccd45\/chat?locale=ru&style=modernnew","preferIFrame":true,"modSecurity":false,"height":464,"width":300,"resizable":true,"styleLoader":"https:\/\/feedback.mcn.ru\/feedback\/chat-57dbd123ccd45\/chat\/style\/popup\/modernnew"});function getMcnCookie(cname) { var name = cname + "=", ca = document.cookie.split(";"), i, c, ca_length = ca.length; for (i = 0; i < ca_length; i += 1) { c = ca[i]; while (c.charAt(0) === " ") { c = c.substring(1); } if (c.indexOf(name) !== -1) { return c.substring(name.length, c.length); } } return ""; }function clickWidget(id, type) {if (!getMcnCookie('mibew-chat-frame-'+id)) { Mibew.Objects.ChatPopups[id].close(); $('.mcntelecom-feedback').find('iframe#chat-first-message').remove(); $('.mcntelecom-feedback').append("<iframe id='chat-first-message' class='mibew-chat-frame' frameborder='0' src='https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/index.php?location=modernnew-first_message&lang=ru&codes=ru' style='display: block;'></iframe>"); }}</script><script type="text/javascript">function isJson(str){try{JSON.parse(str);}catch(e){return false;}return true;} window.addEventListener('message',function(event) { var feedbackDomain = 'https://feedback.mcn.ru'; var originDomain = window.location.protocol+'//'+window.location.hostname; if (!(event.origin == feedbackDomain || event.origin == originDomain)) {return;} if (!isJson(event.data)) {return;} var messageObject = event.data; messageObject = JSON.parse(event.data); if (messageObject.action == 'mibew-cmd') { if (messageObject.data == 'close') { var idChat = Object.keys(Mibew.Objects.ChatPopups)[0]; Mibew.Utils.deleteCookie('mibew-chat-frame-' + idChat); var iframe = document.getElementById('mibew-chat-frame-' + idChat); iframe.parentNode.removeChild(iframe); if (typeof $ !== 'undefined') { if (typeof $('.mcntelecom-from-site-call').html() !== 'undefined') { $('.mcntelecom-from-site-call').removeClass('active'); } } } if (messageObject.data == 'open') { var idChat = Object.keys(Mibew.Objects.ChatPopups)[0]; $('.mcntelecom-feedback').find('iframe#chat-first-message').remove(); Mibew.Objects.ChatPopups[idChat].open(); if (messageObject.first_message) { setTimeout(function(){ Mibew.Objects.ChatPopups[idChat].iframe.contentWindow.postMessage(JSON.stringify({action: 'mibew-cmd', data: 'first_message', first_message: messageObject.first_message}), '*'); }, 2000); } } if (messageObject.data == 'country_code') { var idChat = Object.keys(Mibew.Objects.ChatPopups)[0]; if (Mibew.Objects.ChatPopups[idChat].iframe) { Mibew.Objects.ChatPopups[idChat].iframe.contentWindow.postMessage(JSON.stringify({action: 'mibew-cmd', data: 'country_code', list: ['ru']}), '*'); } } } }, false); setTimeout(function() { }, 1000);</script><div id="mibew-invitation"></div><script type="text/javascript" src="https://feedback.mcn.ru/feedback/chat-57dbd123ccd45/js/compiled/widget.js"></script><script type="text/javascript">Mibew.Widget.init({"inviteStyle":"https:\/\/feedback.mcn.ru\/feedback\/chat-57dbd123ccd45\/styles\/invitations\/default\/invite.css","requestTimeout":10000,"requestURL":"https:\/\/feedback.mcn.ru\/feedback\/chat-57dbd123ccd45\/widget","locale":"ru","visitorCookieName":"MIBEW_VisitorID"})</script><!-- / widget -->