<?
CCurrencyLang::disableUseHideZero();
if ($USER->IsAuthorized()){
    $arUser = BXExtra::UserInfo();
    $PayType = "ИП";
    if($arUser['UF_LEGAL_VALUE']){
        $PayType = $arUser['UF_LEGAL_VALUE'];
    }
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => $PayType);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();  
        $arFields['PROPERTIES'] = $ob->GetProperties();
        $arRequisites = $arFields;
    }
}
else {
    RedirectToRegister();
}
if ($arRequisites){
    if (!empty($_REQUEST['pdf']))
        return include(dirname(__FILE__).'/pdf-legal.php');
    else
        return include(dirname(__FILE__).'/html-legal.php');
}
else {
    if (!empty($_REQUEST['pdf']))
        return include(dirname(__FILE__).'/pdf.php');
    else
        return include(dirname(__FILE__).'/html.php');
}
CCurrencyLang::enableUseHideZero();

?>