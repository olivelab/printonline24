<?
/********************************************************************************
Simple delivery services.
It uses fixed delivery price for any location groups. Needs at least one group of locations to be configured.
********************************************************************************/
CModule::IncludeModule("sale");
//IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/sale/delivery/delivery_simple.php');

class CDeliveryPrint
{
	function Init()
	{
        
		return array(
			/* Basic description */
			"SID" => "print",
			"NAME" => "Доставка print-online24",
			"DESCRIPTION" => "",
			"DESCRIPTION_INNER" => "",
			"BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),

			"HANDLER" => __FILE__,
			
			/* Handler methods */
			"DBGETSETTINGS" => array("CDeliveryPrint", "GetSettings"),
			"DBSETSETTINGS" => array("CDeliveryPrint", "SetSettings"),
			"GETCONFIG" => array("CDeliveryPrint", "GetConfig"),
			
			"COMPABILITY" => array("CDeliveryPrint", "Compability"),			
			"CALCULATOR" => array("CDeliveryPrint", "Calculate"),			
			
			/* List of delivery profiles */
			"PROFILES" => array(
				"print" => array(
					"TITLE" => "Доставка",
					"DESCRIPTION" => "",					
					"RESTRICTIONS_WEIGHT" => array(0),
					"RESTRICTIONS_SUM" => array(0),
				),
			)
		);
	}
	
	function GetConfig()
	{
		$arConfig = array(
			"CONFIG_GROUPS" => array(
				"all" => "Стоимость доставки",
			),
			
			"CONFIG" => array(
                "MOSCOW" => array(
					'TYPE' => 'SECTION',
					'TITLE' => "Москва",
					'GROUP' => 'all',
				),
                "MOSCOW_PRICE" => array(
                    "TYPE" => "STRING",
					"DEFAULT" => "40000",
					"TITLE" => "Цена",
					"GROUP" => "all"
				),
                "MOSCOW_FREE" => array(
                    "TYPE" => "STRING",
					"DEFAULT" => "0",
					"TITLE" => "При заказе более \"Цена\"",
					"GROUP" => "all"
				),
                "MOSCOW_PAY" => array(
                    "TYPE" => "STRING",
					"DEFAULT" => "900",
					"TITLE" => "При заказе менее или равно \"Цена\"",
					"GROUP" => "all"
				),
                "MO" => array(
					'TYPE' => 'SECTION',
					'TITLE' => "Московская область",
					'GROUP' => 'all',
				),
                "MO_PRICE" => array(
                    "TYPE" => "STRING",
					"DEFAULT" => "1500",
					"TITLE" => "Вес",
					"GROUP" => "all"
				),
                "MO_LESS" => array(
                    "TYPE" => "STRING",
					"DEFAULT" => "1800",
					"TITLE" => "Цена за вес",
					"GROUP" => "all"
				),
            ),
		);
		
		return $arConfig; 
	}
	
	function GetSettings($strSettings)
	{
		return unserialize($strSettings);
	}
	
	function SetSettings($arSettings)
	{
		foreach ($arSettings as $key => $value) 
		{
			if (strlen($value) > 0)
				$arSettings[$key] = doubleval($value);
			else
				unset($arSettings[$key]);
		}
	
        
		return serialize($arSettings);
	}

	function __GetLocationPrice($arOrder, $arConfig)
	{
        
		$dbLocationGroups = CSaleLocationGroup::GetLocationList(array("LOCATION_ID" => $arOrder["LOCATION_TO"]));
        $deliveryPrice = 0;
		
		while ($arLocationGroup = $dbLocationGroups->Fetch())
		{
            if ($arLocationGroup["LOCATION_GROUP_ID"] == 2){
                $weight = $arOrder['WEIGHT']/1000;
                $middleWeight = $arConfig['MO_PRICE']['VALUE'];
                $deliveryPrice = ceil($weight/$middleWeight)*$arConfig['MO_LESS']['VALUE'];
            }
            if ($arLocationGroup["LOCATION_GROUP_ID"] == 1){
                $price = $arOrder['PRICE'];
                $middlePrice = $arConfig['MOSCOW_PRICE']['VALUE'];
                if ($price <= $middlePrice){
                    $deliveryPrice = $arConfig['MOSCOW_PAY']['VALUE'];
                }
                else if($price > $middlePrice){
                    $deliveryPrice = $arConfig['MOSCOW_FREE']['VALUE'];
                }
            }
			
		}
    	return $deliveryPrice;

	}
	
	function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
	{
        return CDeliveryPrint::__GetLocationPrice($arOrder, $arConfig);
	}
	
	function Compability($arOrder, $arConfig)
	{
        
		$price = CDeliveryPrint::__GetLocationPrice($arOrder, $arConfig);
        
		if ($price === false)
			return array();
		else
			return array('print');
	} 
}

AddEventHandler("sale", "onSaleDeliveryHandlersBuildList", array('CDeliveryPrint', 'Init')); 
?>