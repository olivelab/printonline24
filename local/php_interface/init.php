<?
define("MEDIA_PATH", "/local/media/");
define("INCLUDE_PATH", "/local/include/");
define("AJAX_PATH", "/local/include/ajax/");
define("PRODUCT_DISCOUNT", ".85");
define("AGENCY_GROUP", "6");
define("MANAGER_GROUP", "7");


function RedirectToRegister(){
    global $USER;
    if (!$USER->IsAuthorized()) {
        LocalRedirect('/personal/register/');
    }
}
CModule::AddAutoloadClasses(
  '', 
  array(
    'ProductDetail' => '/local/php_interface/include/ProductDetail.php',    
    'UserHandlers' => '/local/php_interface/include/User/login.php',  
    'BXExtra' => '/local/php_interface/include/Helper.php'  
  )
);

require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/User/OrderUserType.php");

AddEventHandler("main", "OnBeforeUserLogin", Array("UserHandlers", "OnBeforeUserLogin"));
AddEventHandler("sale", "OnSaleComponentOrderOneStepPersonType", "selectSavedPersonType");


function getRealCurDir() {
    global $APPLICATION;
    $filePath = $_SERVER["REAL_FILE_PATH"];
    $fileArr = explode("/",$filePath);
    array_pop($fileArr);
    $curDir = $_SERVER["REAL_FILE_PATH"] ? implode("/", $fileArr)."/" : $APPLICATION->GetCurDir();
    return $curDir;    
}
function isStartPage() {
    if((getRealCurDir()=="") or (getRealCurDir()=="/")) return true;
    else return false;
}
class OrderTemplate {
    function Add($code, $value, $order) {
      if (!strlen($code)) {
         return false;
      }
      if (CModule::IncludeModule('sale')) {
         if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch()) {
            return CSaleOrderPropsValue::Add(array(
               'NAME' => $arProp['NAME'],
               'CODE' => $arProp['CODE'],
               'ORDER_PROPS_ID' => $arProp['ID'],
               'ORDER_ID' => $order,
               'VALUE' => $value,
            ));
         }
      }
    }
    function Update($id, $value){
        CSaleOrderPropsValue::Update($id, array("VALUE"=>$value));
    }
}
//FormatHelper::plural(count($arResult['ITEMS']), array('позиция', 'позиции', 'позиций'))
class FormatHelper
{
    function normalizeFiles(&$files){  
        $_files = array();
        $_files_count = count( $files[ 'name' ] );
        $_files_keys  = array_keys( $files );
    
        for ( $i = 0; $i < $_files_count; $i++ )
            foreach ( $_files_keys as $key )
                $_files[ $i ][ $key ] = $files[ $key ][ $i ];
    
        return $_files;
    }
    function makePhoneString($phone){
        $string = str_replace(array('+',')','(','-',' '),'',$phone);
        return $string;
    }
    function makePhoneFormat($phone){
        $prefix = substr($phone,0,1);
        $code = substr($phone,1,3);
        $phoneFirst = substr($phone,4,3);
        $phoneSecond = substr($phone,7,2);
        $phoneThird = substr($phone,9,2);
        $string = "(".$code.") ".$phoneFirst."-".$phoneSecond."-".$phoneThird;
        return $string;
    }
    function FileSize($sizeInBytes) {
        $s = array('байт', 'кб', 'Мб', 'Гб', 'Тб', 'Пб');
        $n = floor(log($sizeInBytes, 1024));
        return number_format($sizeInBytes / pow(1024, $n), 1, ',', ' ') . ' ' . $s[$n];
    }
    function dates($dateString) {
        $date = strtotime($dateString);
        $months = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        return date('d', $date) . '  ' . $months[(int) date('m', $date)] .  ', ' . date('H:i');
    }
    function month($dayString) {
        $months = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        return $months[(int) $dayString];
    }
    function day($dayString) {
        $day = array('', 'В понедельник', 'Во вторник', 'В среду', 'В четверг', 'В пятницу', 'В субботу', 'В воскресенье');
        return $day[(int) $dayString];
    }
    function plural($number, $titles){
        $cases = array (2, 0, 1, 1, 1, 2);
        return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
    }
    function price($price){
        $price = number_format($price, 0, ',', ' ');
        return $price;
    }
    function jsonOutput($data, $restartBuffer = true){
		if ($restartBuffer === true) {
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
		}
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
    }
	function Error($text,$word){
		$textString = $text;
		if ($word) $textString = 'Поле "'.$text.'" является обязательным для заполнения!<br>';
		return '<div class="message bg-danger bg-message"><span><i></i>'.$textString.'</span></div>';
	}
	function Success($text){
		if (empty($text)) $text = "Спасибо! Ваша заявка принята!";
		return '<div class="bg-success message bg-message">'.$text.'</div>';
	}
    function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }
    function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= FormatHelper::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = FormatHelper::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.FormatHelper::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }
}
function pr($item, $show_for = false) {
	global $USER;
	if ($USER->IsAdmin() || $show_for == 'all' || $_GET['print_r']) {	
    $bt = debug_backtrace();
    $bt = $bt[0];
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    echo "<div style=\"padding: 3px 5px; background:#99CCFF; font-weight: bold;\">File:".$bt['file']." [".$bt['line']."] </div>";
		if (!$item) echo ' <br />пусто <br />';
		elseif (is_array($item) && empty($item)) echo '<br />массив пуст <br />';
		else echo ' <pre>' . print_r($item, true) . ' </pre>';
	}
}


AddEventHandler("sale", "OnBeforeOrderAdd", "ManagerOrderUserChanger");
function ManagerOrderUserChanger($arFields){
    if ($arFields['ORDER_PROP'][30] || $arFields['ORDER_PROP'][29]) {
        $arFields['USER_ID'] = $arFields['ORDER_PROP'][30]?$arFields['ORDER_PROP'][30]:$arFields['ORDER_PROP'][29];
    }
}