<?
function selectSavedPersonType(&$arResult, &$arUserResult, $arParams)
{
    global $USER;
    if($USER->IsAuthorized())
    {
        $rsUser = $USER->GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
        $entity = $arUser['UF_TYPE'];
        
        if ($arUserResult['PERSON_TYPE_OLD']){
            $entity = $arUserResult['PERSON_TYPE_ID'];
        }
        
        foreach($arResult['PERSON_TYPE'] as $key => $type){
            if($type['CHECKED'] == 'Y'){
                $arResult['PERSON_TYPE'][$key]['CHECKED'] = '';
            }
        }
        $arResult['PERSON_TYPE'][$entity]['CHECKED'] = 'Y';
        $arUserResult['PERSON_TYPE_ID'] = $entity;
    }
}