<?
class BXExtra {
    function UserInfo($ID){
        global $USER;
        if (!$ID) $ID = $USER->GetID();
        $rsUser = CUser::GetByID($ID);
        $arUser = $rsUser->Fetch();
        $arUser['GROUP'] = $USER->GetUserGroup($ID);
        if (!empty($arUser['UF_LEGAL'])){
            $arUser['UF_LEGAL_VALUE'] = BXExtra::UserLegalInfo($arUser['UF_LEGAL']);
        }
        
        return $arUser;
    }
    function UserLegalInfo($ID){
        $rsEnum = CUserFieldEnum::GetList(array(), array(
            "ID" => $ID,
        ));
        if($arEnum = $rsEnum->GetNext())
        {
            return $arEnum['VALUE'];
        }        
    }
    function makePriceMargin($price, $margin){
        $price = $price*PRODUCT_DISCOUNT;
        $result = $price + ($price * ($margin/100));
        return $result;
    }
}