<?
class ProductDetail {
  private $productId;
  private $arResult = array();

  private function makeRequest($options){
    $opts = array('http' =>
      array(
          'method'  => 'POST',
          'header'  => 'Content-type: application/x-www-form-urlencoded',
          'content' => http_build_query($options)
      )
    );
    $context  = stream_context_create($opts);
    $json_result = file_get_contents('http://printonline.su/calc/calc-data.php', false, $context);
    return json_decode($json_result,true);
  }
  private function getContols($options){
    $options['action'] = 'get-controls';
    $options['prod_id'] = $this->productId;
    $arResult = $this->makeRequest($options);
    $this->arResult = $arResult;
  }
    
  public function getResult($id, $options = false){
    $this->productId = $id;
    $this->getContols($options);
    return $this->arResult;
  }
}
?>

