<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

$arOptionsCode = array();
foreach ($_REQUEST['productoptions'] as $optionCode=>$options){
  $arOptionsCode[] = $optionCode;
}
$isExist = false;
$arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE"=>"Y", "PROPERTY_ELEMENT_ID"=>$_REQUEST['ELEMENT_ID'], "PROPERTY_PRODUCT_ID"=>$_REQUEST['PRODUCT_ID']);

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement()){ 
  $arFields = $ob->GetFields();
  $arFields['PROPERTIES'] = $ob->GetProperties();
  $totalCheck = count($_REQUEST['productoptions']);
  $propertyCount = 0;
  foreach ($_REQUEST['productoptions'] as $optionCode=>$options){
    foreach ($arFields['PROPERTIES']['OPTIONS']['VALUE'] as $propertyKey=>$value){
      if ($optionCode == $value){
        $porpertyDesctiption = $arFields['PROPERTIES']['OPTIONS']['DESCRIPTION'][$propertyKey];
        if ($options == $porpertyDesctiption){
          $propertyCount++;
        }
      }
    }
  }
  if($totalCheck == $propertyCount){
    $isExist = true;
    $str_SHORT_URI = $arFields['PROPERTIES']['LINK']['VALUE'];
  }
}
if ($_REQUEST['AGENCY_PERCENT']){
    $isExist = false;
}
if (!$isExist){
    $el = new CIBlockElement;
    $newNameLink = uniqid();
    $arProps = array();
    
    foreach ($_REQUEST['productoptions'] as $key=>$options){
        $arProps['OPTIONS'][] = array('VALUE'=>$key,'DESCRIPTION'=>$options);
    }
    $arProps['LINK'] = $newNameLink;
    $arProps['ELEMENT_ID'] = $_REQUEST['ELEMENT_ID'];
    $arProps['PRODUCT_ID'] = $_REQUEST['PRODUCT_ID'];
    $arLoadProductArray = Array(
        "IBLOCK_ID" => 4,
        "PROPERTY_VALUES" => $arProps,
        "NAME" => $newNameLink,
        "ACTIVE" => "Y",
    );
    if ($_REQUEST['AGENCY_PERCENT']){
        $arLoadProductArray['PROPERTY_VALUES']['PERCENT_USER'] = $USER->GetID();
        $arLoadProductArray['PROPERTY_VALUES']['AGENCY_PERCENT'] = $_REQUEST['AGENCY_PERCENT'];
    }
    
    if($el->Add($arLoadProductArray))
        $str_SHORT_URI = $newNameLink;
    else
        echo "Error: ".$el->LAST_ERROR;
}
?>
Короткая ссылка на эту страницу (<a class="js-copyText" data-copy="http://<?=$_SERVER['HTTP_HOST'];?>/products/product-<?=$str_SHORT_URI?>/">Копировать ссылку</a>):<br />
<a href="http://<?=$_SERVER['HTTP_HOST'];?>/products/product-<?=$str_SHORT_URI?>/">http://<?=$_SERVER['HTTP_HOST'];?>/products/product-<?=$str_SHORT_URI?>/</a><br>
