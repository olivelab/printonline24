<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("sale");
$ORDER = $orderId?$orderId:$_REQUEST['order']['id'];
$isLoaded = false;
$UploadedFiles = array();
$db_props = CSaleOrderPropsValue::GetOrderProps($ORDER);
while ($arProps = $db_props->Fetch()) {
    if ($arProps['CODE'] == "MAKET") $isLoaded = $arProps;
}
if ($isLoaded['VALUE']) {
    $UploadedFiles = unserialize($isLoaded['VALUE']);
}
if ($_FILES['order']){
    $arFile = FormatHelper::normalizeFiles($_FILES['order']);
    $arSavedId = array();
    foreach ($arFile as $saveFile){
        $arSavedId[] = CFile::SaveFile($saveFile, "order_mockup/".$ORDER."/");
    }
    $noError = true;
    foreach ($arFile as $file){
        if ($file['error']) {
            $noError = false;
            break;
        }  
    }
    if ($noError) {
        if (!$isLoaded){
            OrderTemplate::Add("MAKET", $arSavedId, $ORDER);
        }
        else {
            if (empty($UploadedFiles))
                $UploadedFiles = $arSavedId;
            else 
                $UploadedFiles = array_merge($UploadedFiles,$arSavedId);

            OrderTemplate::Update($isLoaded['ID'],serialize($UploadedFiles));
        }
    }
    
    $db_props = CSaleOrderPropsValue::GetOrderProps($ORDER);
    while ($arProps = $db_props->Fetch()) {
        if ($arProps['CODE'] == "MAKET") $isLoaded = $arProps;
    } 
    if ($isLoaded['VALUE']) {
        $UploadedFiles = unserialize($isLoaded['VALUE']);
    }
}
if (empty($_FILES) && $_REQUEST['order']['ajax']){
    $key = array_search($_REQUEST['order']['ajax'], $UploadedFiles);
    CFile::Delete($UploadedFiles[$key]);
    unset($UploadedFiles[$key]);
    if (count($UploadedFiles)) {
        $loadValue = serialize($UploadedFiles); 
        OrderTemplate::Update($isLoaded['ID'], $loadValue);       
    }
    else {
        unset($UploadedFiles);
        $loadValue = " ";
        CSaleOrderPropsValue::Delete($isLoaded['ID']);
    }
}
?>
<? if ($isLoaded):?>
    <? 
    foreach ($UploadedFiles as $key=>$fileId):
        $uploadedFile = CFile::GetFileArray($fileId);
    ?>
    <form class="upload_tmpl js-upload" method="post" enctype="multipart/form-data" action="<?=AJAX_PATH;?>upload_template.php">
        <?
        if (CFile::IsImage($uploadedFile["FILE_NAME"], $uploadedFile["CONTENT_TYPE"])):
            $bigImg = CFile::GetPath($fileId);
            $file = CFile::ResizeImageGet($fileId, array('width'=>250, 'height'=>250), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
        <a href="<?=$bigImg;?>" class="fancybox"><img src="<?=$file['src'];?>" alt=""></a>
        <? else:?>        
        <a href="<?=$uploadedFile['SRC'];?>" target="_blank"><?=$uploadedFile['ORIGINAL_NAME'];?></a>
        <? endif;?>
        <? if ($status != "G"):?>
        <div><button type="submit">Удалить</button></div>
        <input type="hidden" value="<?=$fileId;?>" name="order[ajax]">
        <input type="hidden" value="<?=$ORDER;?>" name="order[id]">
        <? endif;?>
    </form>    
    <? endforeach;?>
<? endif;?>
<? if ($status != "G"):?>
<form class="upload_tmpl js-upload" method="post" enctype="multipart/form-data" action="<?=AJAX_PATH;?>upload_template.php">
    <? if (!$UploadedFiles):?><div>Загрузить макет</div><? endif;?>
    <div class="upload_tmpl">
        <input type="file" value="" name="order[]">
    </div>
    <input type="hidden" value="<?=$ORDER;?>" name="order[id]">
</form>
<? endif;?>