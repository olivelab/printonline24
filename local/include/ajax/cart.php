<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
if ($_REQUEST['PRODUCT_ID']){
  $_SESSION['USER']['CART'] = $_REQUEST;
}
if ($_REQUEST['delete']){
  unset($_SESSION['USER']['CART']);
}
ob_start();
$APPLICATION->IncludeFile(INCLUDE_PATH.'/template/cart.php', Array("CartPrice"=>number_format($_SESSION['USER']['CART']['PRODUCT_TOTAL_PRICE'],2,',',' ').' руб.'), Array("MODE"=> "html"));
$basket = ob_get_contents();
ob_end_clean();

echo json_encode(array('basket'=>$basket));