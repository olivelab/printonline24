<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
CJSCore::Init(array('ajax', 'window')) ;?>
<div class="popup-form">
  <? $APPLICATION->IncludeComponent("dev:system.auth.form", "popup", array(
    "SHOW_ERRORS" => "Y",
    "AJAX_MODE" => "Y",
    "FORGOT_PASSWORD_URL" => "/auth/?forgot_password=yes",
    "REGISTER_URL" => "/auth/?register=yes",
    "PROFILE_URL" => "/profile/",
  ));?>
</div>