<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/dompdf/autoload.inc.php");

$PdfData = $_SESSION['USER']['PDF'];
$PRODUCT_ID = $PdfData['PRODUCT_ID'];
$requestOptions = $PdfData['productoptions'];
$product = new ProductDetail;
$arResult = $product->getResult($PRODUCT_ID,$requestOptions);
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->set_base_path('/');
$dompdf->set_option('defaultFont', '');
$dompdf->set_option('isRemoteEnabled', TRUE);

$dompdf->set_option('chroot', '/');
$IsAgancy = false;
$arUser = BXExtra::UserInfo();
if (in_array(AGENCY_GROUP,$arUser['GROUP'])){
    $IsAgancy = true;
    $arResult['total-price'] = $arResult['total-price']*PRODUCT_DISCOUNT;
    $MarginTotalPrice = $PdfData['AGENCY_PERCENT'];
    if ($MarginTotalPrice){
        $arResult['total-price'] = $arResult['total-price'] + ($arResult['total-price'] * ($MarginTotalPrice/100));
    }
}
if (in_array(MANAGER_GROUP,$arUser['GROUP'])){
    $arResult['total-price'] = $arResult['total-price']*PRODUCT_DISCOUNT;
    $MarginTotalPrice = $PdfData['AGENCY_PERCENT'];
    if ($MarginTotalPrice){
        $arResult['total-price'] = $arResult['total-price'] + ($arResult['total-price'] * ($MarginTotalPrice/100));
    }
}
ob_start();
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Счет на заказ | <?=$arResult['prod_name']?></title>
        <style type="text/css">
            body {font-family:DejaVu Sans;}
            .h1bask {
                font:16px helveticaneuecyr-bold !important;
                color: #0f5d81;
                margin:0 0 10px;
            }
            font.h1bask {
                font:14px helveticaneuecyr-light !important;
                display:block;
            }
            .order_contact_text {
                color:#8c0412;
                font:16px helveticaneuecyr-roman !important;
            }
            .prod_konf_head_text_order {
                font:16px helveticaneuecyr-bold !important;
                color:#000;
            }
            .prod_konf_head_text_order.order_text_18,
            .prod_konf_head_text_order.order_contact_color_text {
                font-size:16px !important;
                color:#8c0412
            }
            .clear {
                clear:both;
                display:block;
             }
            .left {float:left; display:block;}
            .right {float:right; display:block;}
            .table-simple {
                    font:10px/16px  helveticaneuecyr-light;
            }
                .table-simple > tbody > tr > td {
                    padding:10px 20px !important;
                    border:1px solid #719fb7 !important;
                    border-collapse:collapse;
                }
            
            .total-order {
                background:#126086;
                color:#fff;
            }
                .total-order td {
                    padding:0 20px 10px;
                    vertical-align:top;
                    font-size:12px;
                }
                    .total-order td h2 {
                        font-size:16px;
                        margin:0;
                        padding:0;
                    }
                    .total-order td small {
                        font-size:12px;
                    }
                    .total-order td div {
                        font:18px helveticaneuecyr-bold !important;
                    }
        </style>
    </head>
    <body>
        <div class="holder" style="width:100%; border: 0px solid blue; padding:0px;">
            <div class="prod_s_head_div" style="border:0px solid red; margin:0px;width:100%;">
		    
                <table border="0" style="width: 100%;">
                    <tr>
                        <td align="left" valign="bottom">
                            <? if ($IsAgancy):?>
                            <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?><?=CFile::GetPath($arUser['WORK_LOGO']);?>" border="0" height="60">
                            <? else:?>
                            <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/logo.gif" border="0" height="60">
                            <? endif;?>
                        </td>
                        <td align="right" valign="top">
                            <div style="width:100%;text-align:right;" class="order_contact_text">
                            <? if ($IsAgancy):?>
                                Тел.: <?=$arUser['WORK_PHONE'];?>
                            <? else:?>
                                +7 495 105 95 38<br>info@print-online24.ru
                            <? endif;?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                        <td></td>
                    </tr>
		    
                    <tr>		    
                        <td align="center" valign="bottom" width="70%" colspan="2">
                            <h1 class="h1bask">Коммерческое предложение от компании <? if ($IsAgancy):?><?=$arUser['UF_LEGAL_VALUE'];?> "<?=$arUser['WORK_COMPANY'];?>"<? else:?>Полиграфия Онлайн<? endif;?></h1>
                            <font class="h1bask">действительно на <?=$PdfData['DATE'];?></font>
                        </td>
                    </tr>
                    <tr><td valign="bottom" align="justify" colspan="2" style="padding-top:10px;">
		    <a class="prod_konf_head_text_order order_contact_text order_text_18 no_underline" href="mailto:siteshop@print-online24.ru" style="text-decoration:none;"><b>Внимание!
		    </b></a><br> 
		    <a class="prod_konf_head_text_order order_contact_text order_text_16 order_text_black" href="mailto:siteshop@print-online24.ru" style="text-decoration:none;"><b>Для своевременной отгрузки необходимо заполнить реквизиты Вашей компании в личном кабинете или выслать карточку организации ответным письмом.</b></a>
		    
		    
		    </td></tr>
                </table>
            </div>
            
        <div class="order_basic" style="padding:0px; margin-top:0px;margin-bottom:0px; width:100%; margin-top:0px;">
            <div class="p_ch" style="margin-top: 0px; width:100%;padding-bottom:10px;">
	            <br><br>
                <font class="prod_konf_head_text_order order_contact_color_text">
                    <b><?=$arResult['prod_name']?></b>
                </font>
                <br><br>
                <table class="order_basic-list table-simple" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="500px">
                            <div>
                                <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <? 
                                  foreach ($arResult['base-option'] as $option):
                                  $selected = $requestOptions[$option['name']];
                                  
                                  ?>
                                    <tr>
                                        <td width="29%"><?=$option['title'];?></td>
                                        <? foreach ($option['options'] as $k=>$select):?>
                                          <? if ($selected == $select['value']):?>
                                            <td width="49%"><?=$select['title'];?></td>
                                          <? endif;?>
                                          <? endforeach;?>
                                    </tr>
                                  <? 
                                  endforeach;?>
                                </table>
                            </div>
                            <br>
                       </td>
                       <td valign="bottom">
                            <div>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                        <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                        </td>
                                        <td align="right" width="70" nowrap style='white-space:nowrap;'><?=number_format($arResult['base-price'],2,',',' ');?></td>
                                    </tr>
                                </table>
                            </div>
                       </td>
                   </tr>   
                   
                   <tr>
                    <td>
                        <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <? 
                            foreach ($arResult['ext-options'] as $option):
                            $selected = $requestOptions[$option['name']];
                            ?>
                              <tr>
                                <td width="29%"><?=$option['title'];?></td>
                                <? foreach ($option['options'] as $k=>$select):?>
                                <? if ($selected == $select['value']):?>
                                  <td width="49%"><?=$select['title'];?></td>
                                <? endif;?>
                                <? endforeach;?> 
                              </tr>
                            <? endforeach;?>
                        </table>
                    </td>    
                    <td valign="top">
                        <? 
                        foreach ($arResult['ext-options'] as $option):
                        ?>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right">
                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                </td>
                                <td align="right" width="70" nowrap style='white-space:nowrap;'><?=number_format($option['price'],2,',',' ');?></td>
                            </tr>
                        </table>
                        <? endforeach;?>
                    </td>                
                   </tr>
                    <tr>
                        <td colspan="2">    
                            <table class="order_basic-table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="29%">Стоимость продукции (Включая НДС) </td>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                <img src="<?=$_SERVER['REQUEST_SCHEME'];?>://<?=$_SERVER['HTTP_HOST'];?>/local/media/images/rubel-g.gif" border="0" height="10" width="9">
                                                </td>
                                                <td align="right" width="70" nowrap style='white-space:nowrap;'><?=number_format($arResult['total-price'],2,',',' ');?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                 
                </table>
                <table class="total-order" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top"><h2>Общая сумма:</h2></td>
                        <td align="right">
                            <div><?=number_format($arResult['total-price'],2,',',' ');?> руб.</div>
                            <small><?=FormatHelper::num2str($arResult['total-price']);?></small>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><br><!--<div>Общий вес: <?=number_format($arResult['total-weight'],2,',',' ');?> кг.</div><br>--><div>Тираж: <?=floor($requestOptions['PRINT_RUN']);?> шт.</div></td>
                    </tr>
                </table>
            </div>             
        </div>
    </body>
</html>
<?
$html = ob_get_contents();
ob_end_clean();
$dompdf->loadHtml($html);
$dompdf->setPaper('A4');
$dompdf->render();
$dompdf->stream('print-online24'.time(),array('Attachment'=>1));