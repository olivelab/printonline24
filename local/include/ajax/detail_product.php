<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$PRODUCT_ID = $PRODUCT_ID;

$requestOptions = false;
if($linkOptions){
  $requestOptions = $linkOptions;
}
if (!empty($_REQUEST['PRODUCT_ID']) && !empty($_REQUEST['ELEMENT_ID'])){
  $PRODUCT_ID = $_REQUEST['PRODUCT_ID'];
  $requestOptions = $_REQUEST['productoptions'];
}
$product = new ProductDetail;
$arResult = $product->getResult($PRODUCT_ID,$requestOptions);

/*If user is agancy*/
global $USER;
$MarginTotalPrice = false;
$PrintRun = 0;
$IsAgancy = false;
if ($USER->IsAuthorized()){
    $arUser = BXExtra::UserInfo();
    if (in_array(AGENCY_GROUP,$arUser['GROUP']) || in_array(MANAGER_GROUP,$arUser['GROUP'])){
        $IsAgancy = true; 
        $MarginTotalPrice = $_REQUEST['AGENCY_PERCENT']?$_REQUEST['AGENCY_PERCENT']:$arUser['UF_MARGIN'];
        $PriceWithOutMargin = $arResult['total-price']*PRODUCT_DISCOUNT;
        if ($PriceWithOutMargin){
            $arResult['total-price'] = BXExtra::makePriceMargin($arResult['total-price'], $MarginTotalPrice);
            $arResult['base-price'] = BXExtra::makePriceMargin($arResult['base-price'], $MarginTotalPrice);
        }
    }
}
if ($agentPercent || !$IsAgancy && $_REQUEST['AGENCY_PERCENT']){
    /*PRICE MARGIN FOR LINK*/
    $MarginTotalPrice = $_REQUEST['AGENCY_PERCENT']?$_REQUEST['AGENCY_PERCENT']:$agentPercent;
    
    $arResult['total-price'] = BXExtra::makePriceMargin($arResult['total-price'], $MarginTotalPrice);
    $arResult['base-price'] = BXExtra::makePriceMargin($arResult['base-price'], $MarginTotalPrice);
}

?>
<form class="product_form js-add_cart" method="post">
    <input type="hidden" value="<?=$PRODUCT_ID;?>" name="PRODUCT_ID">
    <input type="hidden" value="<?=$_REQUEST['ELEMENT_ID']?$_REQUEST['ELEMENT_ID']:$ELEMENT_ID;?>" name="ELEMENT_ID">
    <? if ($agentPercent || !$IsAgancy && $_REQUEST['AGENCY_PERCENT']):?><input type="hidden" value="<?=$_REQUEST['AGENCY_PERCENT']?$_REQUEST['AGENCY_PERCENT']:$agentPercent;?>" name="AGENCY_PERCENT"><? endif;?>
    <? 
    $currentPrice = $arResult['total-price'];
    if ($_REQUEST['productdelivery']['price']){
        $currentPrice = $arResult['total-price'];
        $_REQUEST['productdelivery']['price'] = $arResult['total-price']*0.2;
        $arResult['total-price'] += $arResult['total-price']*0.2;
    }
    ?>  
    <div 
        class="price-array"
        data-current_format="<?=number_format($currentPrice,2,',',' ');?>"
        data-fast_format="<?=number_format($currentPrice + ($currentPrice*0.2),2,',',' ');?>"
        data-procent_format="<?=number_format($currentPrice*0.2,2,',',' ');?>"
        data-fast="<?=number_format($currentPrice + ($currentPrice*0.2),2,',',' ');?>"
        data-current="<?=$currentPrice;?>"
    ><? /*FOR DATE CALCULATE*/?>
    </div>
    <input type="hidden" value="<?=$arResult['prod_name'];?>" name="PRODUCT_NAME">
    <input type="hidden" value="<?=$arResult['total-price'];?>" name="PRODUCT_TOTAL_PRICE">
    <input type="hidden" value="<?=$arResult['base-price'];?>" name="PRODUCT_BASE_PRICE">
    <input type="hidden" value="<?=$arResult['total-weight'];?>" name="PRODUCT_WEIGHT">
    <input type="hidden" value="<?=FormatDate('j F Y H:i:s', time());?>" name="DATE">
    <div class="product-detail_config">
      <h3>Конфигурация продукта</h3>
      <div class="product-form">
        <div class="product-form_bg">
        <? 
        foreach ($arResult['base-option'] as $option):
            if ($option['name'] == 'PRINT_RUN') $PrintRun = $option['options'][0]['value'];
            $selected = false;
            if ($requestOptions){
              $selected = $requestOptions[$option['name']];
            }
            $FieldType = "select";
            if (
                $PRODUCT_ID == 118 && $option['name'] == "TYPE" ||
                $PRODUCT_ID == 8 && $option['name'] == "SIZE" ||
                $PRODUCT_ID == 10 && $option['name'] == "SIZE" ||
                $PRODUCT_ID == 34 && $option['name'] == "SIZE" ||
                $PRODUCT_ID == 35 && $option['name'] == "SIZE" ||
                $PRODUCT_ID == 77 && $option['name'] == "SIZE" ||
                $PRODUCT_ID == 65 && $option['name'] == "TYPE" ||
                $PRODUCT_ID == 66 && $option['name'] == "TYPE" ||
                $PRODUCT_ID == 67 && $option['name'] == "TYPE" ||
                $PRODUCT_ID == 51 && $option['name'] == "TYPE"
            ){
                $FieldType = "example";
            }        
            ?>
            <? if ($FieldType == "example"):?>
            <div class="clearfix product-form_control">
                <div class="product-form_control-name left">
                    <span><?=$option['title'];?><i data-fancybox-href="<?=AJAX_PATH;?>help_detail.php?help=<?=$option['help-id'];?>" class="js-popUp fancybox.ajax"></i></span>
                </div>
                <div class="product-form_control-input left">
                    <?
                    global $ArTitles;
                    $ArTitles = array();
                    foreach ($option['options'] as $k=>$select){
                        $ArTitles['NAME'][] = $select['title'];
                    }
                    ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "example-select",
                        Array(
                            "SELECTED" => $selected,
                            "OPTION"=> $option,
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "COMPONENT_TEMPLATE" => ".default",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "N",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("", ""),
                            "FILTER_NAME" => "ArTitles",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "9",
                            "IBLOCK_TYPE" => "directory",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "200",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array("", ""),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "ASC"
                        )
                    );?>
                </div>
            </div>
            <? else:?>
            <div class="clearfix product-form_control">
                <div class="product-form_control-name left">
                    <span><?=$option['title'];?><i data-fancybox-href="<?=AJAX_PATH;?>help_detail.php?help=<?=$option['help-id'];?>" class="js-popUp fancybox.ajax"></i></span>
                </div>
                <div class="product-form_control-input left">
                    <select class="chosen js-product_detail-update" name="productoptions[<?=$option['name'];?>]">
                    <? foreach ($option['options'] as $k=>$select):?>
                        <option value="<?=$select['value'];?>"<? if ($k==0 && !$selected || $selected == $select['value']):?>selected<?  endif;?>><?=$select['title'];?></option>
                    <? endforeach;?>
                    </select>
                </div>
            </div>
            <? endif;?>
        <? endforeach;?>
        </div>
        <div class="product-form-total clearfix">
            <div class="right text-right">Базовая стоимость продукции: <b><?=number_format($arResult['base-price'],2,',',' ');?> <span class="rur">a</span></b></div>
        </div>
      </div>
    </div>
    <div class="product-detail_config">
      <h3>Дополнительные опции</h3>
      <div class="product-form">
        <div class="product-form_bg orange-bg">
          <?
          $timeValue = "";
          foreach ($arResult['ext-options'] as $k=>$option):
            $selected = false;
            if ($requestOptions){
                $selected = $requestOptions[$option['name']];
            }
            $extPrice = $option['price'];
            
            $FieldType = "select";
            if (
                $PRODUCT_ID == 75 && $option['name'] == "EXT_NUMBER_FOLD"
            ){
                $FieldType = "example";
            }
          ?>
          <? if ($FieldType == "example"):?>
            <div class="clearfix product-form_control">
                <div class="product-form_control-name left">
                    <span><?=$option['title'];?><i data-fancybox-href="<?=AJAX_PATH;?>help_detail.php?help=<?=$option['help-id'];?>" class="js-popUp fancybox.ajax"></i></span>
                </div>
                <div class="product-form_control-input left">
                <?
                global $ArTitles;
                $ArTitles = array();
                foreach ($option['options'] as $k=>$select){
                    $ArTitles['=NAME'][] = $select['title'];
                }
                ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "example-select",
                    Array(
                        "SELECTED" => $selected,
                        "OPTION"=> $option,
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "COMPONENT_TEMPLATE" => ".default",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("", ""),
                        "FILTER_NAME" => "ArTitles",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "9",
                        "IBLOCK_TYPE" => "directory",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "200",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("", ""),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "ASC"
                    )
                );?>
                </div>
            </div>
            <? else:?>
          <div class="clearfix product-form_control">
            <div class="product-form_control-name left">
                <span>
                    <?=$option['title'];?>
                    <i data-fancybox-href="<?=AJAX_PATH;?>help_detail.php?help=<?=$option['help-id'];?>" class="js-popUp fancybox.ajax"></i>
                    <?
                    if ($extPrice):
                    if ($MarginTotalPrice){
                        $extPrice = BXExtra::makePriceMargin($extPrice, $MarginTotalPrice);
                    }
                    ?>
                    <small>(<?=number_format($extPrice,2,',',' ');?> руб.)</small>
                    <? endif;?>
               </span>
            </div>
            <div class="product-form_control-input left">
              <select class="chosen js-product_detail-update" name="productoptions[<?=$option['name'];?>]">
              <? 
              foreach ($option['options'] as $k=>$select):
              if ($option['name'] == "EXT_TIME"){
                if ($k==0 && !$selected || $selected == $select['value']){
                  $timeValue = $select['title'];
                }
              }
              ?>
                <option value="<?=$select['value'];?>"<? if ($k==0 && !$selected || $selected == $select['value']):?>selected<? endif;?>><?=$select['title'];?></option>
              <? endforeach;?>
              </select>
            </div>
          </div>
          <? endif;?>
          <? endforeach;?>
          <? 
          if (!empty($arResult['variables'])):
              $fastDays = $arResult['variables'][0]['value'];
              $normalDays = $arResult['variables'][1]['value'];
              if (date('H') > 16) {
                  $fastDays += 1;
                  $normalDays += 1;
              }
              $defaultDate = new DateTime("+".$normalDays." day");
          ?>
            <div class="clearfix product-form_control">
                <div class="product-form_control-name left"><span>Срок изготовления<i deta-help-id="4"></i><small id="dateActionPrice"><? if ($_REQUEST['productdelivery']['price']):?>(<?=number_format($_REQUEST['productdelivery']['price'],2,',',' ');?> руб.)<? endif;?></small></span></div>
                <div class="product-form_control-input left">
                    <div></div>
                    <input type="text" readonly id="datepicker" class="form-control form-datepicker" name="productdelivery[date]" value="<?=$defaultDate->format('d.m.Y');?>">
                    <input type="hidden" id="dateAction" name="productdelivery[price]" value="<?=$_REQUEST['productdelivery']['price'];?>">
                </div>
            </div>
            <script>
            
                $(function(){
                    var fastPrint = <?=$fastDays;?>,
                        normalPrint = <?=$normalDays;?>,
                        endDate = fastPrint + normalPrint;
                    $('#datepicker').datepicker({
                        language: "ru",
                        format: "dd.mm.yyyy",
                        forceParse:false,
                        autoclose: true,
                        startDate:'+<?=$fastDays;?>d',
                        endDate: '+<?=$fastDays+31;?>d',
                        assumeNearbyYear:true,
                        maxViewMode:'month',
                        todayHighlight: true,
                        beforeShowDay:function(date){
                            var currentDate = new Date(),
                                normalPrintTime = (currentDate.getTime()/1000)+((normalPrint-1)*86400);      
                                
                            if (date.getTime()/1000 <= normalPrintTime){
                                return "fast-print";
                            }
                            else {
                                return "normal-print";
                            }
                        },
                    }).on("changeDate", function(e) {
                        var date = e.date,
                            currentDate = new Date(),
                            normalPrintTime = (currentDate.getTime()/1000)+((normalPrint-1)*86400);
                            price = $('.price-array'),
                            priceProcent = price.data('procent_format'),
                            priceCurrent = price.data('current_format'),
                            priceFast = price.data('fast_format'),
                            priceFastInt = price.data('fast'),
                            priceCurrentInt = price.data('current');
                        if (date.getTime()/1000 <= normalPrintTime){
                            $('#dateAction').val(priceProcent);
                            $('#dateActionPrice').text('('+priceProcent+' руб.)');
                            $('.total-price').text(priceFast);
                            $('input[name="PRODUCT_TOTAL_PRICE"]').val(priceFastInt);
                        }
                        else {
                            $('#dateAction').val("")
                            $('#dateActionPrice').text("");
                            $('.total-price').text(priceCurrent);
                            $('input[name="PRODUCT_TOTAL_PRICE"]').val(priceCurrentInt); 
                        }                      
                    });                   

                });
            </script>
          <? endif;?>
          <div class="clearfix product-form_control">
            <div class="product-form_control-name left"></div>
            <div class="product-form_control-input left">
              Оплата и макет до 22:00 часов и мы приступаем в этот же день
            </div>
          </div>
          
        </div>
        <div class="product-form_delivery">
        <h4>Доставка возможна:</h4>
        <? 
          if (!empty($arResult['variables'])):
          $fastDays = $arResult['variables'][0]['value'];
          if (date('H') > 16) $fastDays += 1;
          $makeDate = new DateTime("+".$fastDays." day");
          $makeDate->format('d.m.Y');
          ?>
        <p><?=FormatHelper::day($makeDate->format('N'));?>, <?=$makeDate->format('d-го');?> <?=FormatHelper::month($makeDate->format('n'));?> <?=$makeDate->format('Y');?> года (в зависимости от оплаты и готовности макета к печати)<br>Посмотрите <a href="/how-to-order/how-to-prepare-the-layout-for-printing/">как подготовить макет к печати</a>, также <a href="/shipping-and-payment/payment-methods/">«способы оплаты»</a></p>
        <? endif;?>
        </div>
        <div class="product-form-total clearfix">
            <div class="left">
            <table>
              <tr>
                <td class="text-right">Вес тиража:</td>
                <td><b><?=number_format($arResult['total-weight'],2,',',' ');?> кг</b></td>
              </tr>
            </table>
            </div>
            <div class="right text-right">
                Стоимость продукции: <b><span class="total-price"><?=number_format($arResult['total-price'],2,',',' ');?></span> <span class="rur">a</span></b>
                <?
                if ($PrintRun):
                    if ($requestOptions['PRINT_RUN']) $PrintRun = $requestOptions['PRINT_RUN'];
                    $PrintRun = (int)$PrintRun;
                ?>
                <div><small>цена за единицу: <?=number_format($arResult['total-price']/$PrintRun,2,',',' ');?> <span class="rur">a</span></small></div>
                <? endif; ?>
            </div>
        </div>
      </div>
    </div>
    <? if ($IsAgancy):?>
    <div class="clearfix">  
        <div class="text-left right" style="width:197px;">
            <label>Наценка</label>
            <div class="form-control-select">
                <select class="chosen js-product_detail-update" name="AGENCY_PERCENT">
                <? for ($i=0;$i<=100;$i+=5):?>
                <option value="<?=$i;?>"<? if ($MarginTotalPrice == $i):?> selected<? endif;?>><?=$i;?>%</option>
                <? endfor;?>
                </select>
            </div>
            <p><small>цена без наценки <?=number_format($PriceWithOutMargin,2,',',' ');?> <span class="rur">a</span></small></p>
        </div>            
    </div>            
    <? endif;?>
    <div class="form-action text-right">
      <button class="btn btn-green">В корзину</button>
    </div>
  </form>    
  <div class="short_link-url"></div>
  <div class="product-detail_action">
    <a href="#" class="pdf-link js-make_pdf">Предложение в PDF / печать</a><!--
    --><span></span><!--
<!--    <a href="#" class="fav-link">Добавить в избранное</a><span></span>--><!--
    --><a href="#" class="href-link js-short_link">Прямая ссылка</a>
  </div>