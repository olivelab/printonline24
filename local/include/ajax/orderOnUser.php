<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
$rsUserLegal = CUserFieldEnum::GetList(array(), array());
if($arLegalSingle = $rsUserLegal->GetNext()){
    $arLegal[] = $arLegalSingle;
}
$userFilter = array(
    "!GROUPS_ID" => array(1,AGENCY_GROUP,MANAGER_GROUP,8)
);
$rsUsers = CUser::GetList(
    ($by="LOGIN"), 
    ($order="asc"), 
    $userFilter, 
    array(
        "SELECT"=>array("UF_*")
    )
);
$arUsersList = array();
while ($arUserFetch = $rsUsers->Fetch()) {
    $arUsersList[$arUserFetch['ID']] = $arUserFetch;
}
//pr($arResult["PERSON_TYPE"]);
//pr($_REQUEST);
?>
<div class="form-control-select">
    <select name="USER_NEW_ID" class="chosen-search" onChange="submitForm('USER');">
        <option value="">Пользователи</option>
        <? 
        foreach($arUsersList as $arUserSelect):
        $fullName = array();
        if ($arUserSelect['UF_TYPE'] == 1) {
            if ($arUserSelect['LAST_NAME']) 
                $fullName[] = $arUserSelect['LAST_NAME'];
            if ($arUserSelect['NAME']) 
                $fullName[] = $arUserSelect['NAME'];
            if ($arUserSelect['SECOND_NAME']) 
                $fullName[] = $arUserSelect['SECOND_NAME'];
        }
        if ($arUserSelect['UF_TYPE'] == 2){
            if ($arUserSelect['UF_LEGAL']) {
                $fullName[] = BXExtra::UserLegalInfo($arUserSelect['UF_LEGAL']);
            }
            $fullName[] = '"'.$arUserSelect['WORK_COMPANY'].'"';
        }
        $login = is_numeric($arUserSelect["LOGIN"])?"+7" . FormatHelper::makePhoneFormat($arUserSelect['LOGIN']):$arUserSelect["LOGIN"];
        ?>
            <option data-type="<?=$arUserSelect['UF_TYPE'];?>" value="<?=$arUserSelect['ID'];?>"<? if ($_REQUEST['USER_NEW_ID'] == $arUserSelect['ID']):?> selected<? endif;?>>
                <?=$arUserSelect['ID'];?>: <?=implode(" ", $fullName);?> [<?=$login;?>, <?=$arUserSelect['EMAIL'];?>] - <?=($arUserSelect['UF_TYPE']==1)?"Физическое лицо":"Юридическое лицо"?>
            </option>
        <? endforeach;?>
    </select>
</div>
<? 
if ($_REQUEST['USER_NEW_ID']){
    $NewUser = $arUsersList[$_REQUEST['USER_NEW_ID']];
    $NewUserType = $NewUser['UF_TYPE'];
    $_REQUEST['PERSON_TYPE'] = $NewUserType;
}