<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
CJSCore::Init(array('ajax', 'window', 'date')) ;?>
<h2 class="text-center">Регистрация</h2>
<?$APPLICATION->IncludeComponent(
	"dev:manager.register",
	"",
	Array(
		"AUTH" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE", "WORK_COMPANY"),
		"SUCCESS_PAGE" => "/",
        "IND_FIELDS_SHOW" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE","UF_TYPE"),
        "ENT_FIELDS_SHOW" => array("PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET","WORK_PHONE", "UF_LEGAL"),
        "IND_FIELDS_REQUIRED" => array("PERSONAL_MOBILE", "EMAIL", "NAME","UF_TYPE"),
        "ENT_FIELDS_REQUIRED" => array( "PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET", "UF_LEGAL"),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "N",
        "AJAX_MODE" => "Y",
        "REDIRECT"=>"/",
        "AJAX_OPTION_JUMP" => "Y",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
	)
);?>