<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
CJSCore::Init(array('ajax', 'window')) ;?>
<div class="popup-form">
  <?
    $APPLICATION->IncludeComponent(
    "bitrix:system.auth.forgotpasswd","popup",
    array(
        "SHOW_ERRORS" => "Y",
        "AJAX_MODE" => "Y"
    ), 
    false
);?>
</div>