<?
global $USER;
//pr($_SESSION);

CModule::IncludeModule('iblock');
$arOptionsCode = array();
/*
$PrintRun = floor($_SESSION['USER']['CART']['productoptions']['PRINT_RUN']);
$PrintPricePerItem = $_SESSION['USER']['CART']['PRODUCT_TOTAL_PRICE']/$PrintRun;
$PritWeightPerItem = ($_SESSION['USER']['CART']['PRODUCT_WEIGHT'] / $PrintRun) * 1000;
pr ($PrintPricePerItem);
pr ($_SESSION['USER']['CART']['PRODUCT_TOTAL_PRICE']);
pr ($PrintRun);
*/

$PrintRunValue = floor($_SESSION['USER']['CART']['productoptions']['PRINT_RUN']);
$CartQuantity = 1;
$PrintPricePerItem = $_SESSION['USER']['CART']['PRODUCT_TOTAL_PRICE'];
$PritWeightPerItem = $_SESSION['USER']['CART']['PRODUCT_WEIGHT'] * 1000;

foreach ($_SESSION['USER']['CART']['productoptions'] as $optionCode=>$options){
  $arOptionsCode[] = $optionCode;
}
$InIblock = false;
$arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*");
$arFilter = Array(
    "IBLOCK_ID"=>8, 
    "ACTIVE"=>"Y", 
    "PROPERTY_ELEMENT_ID"=>$_SESSION['USER']['CART']['ELEMENT_ID'], 
    "PROPERTY_PRODUCT_ID"=>$_SESSION['USER']['CART']['PRODUCT_ID'], 
    "PROPERTY_MADE"=>$_SESSION['USER']['CART']['productdelivery']['date'], 
    "PROPERTY_MADE_PRICE"=>$_SESSION['USER']['CART']['productdelivery']['price'], 
    "PROPERTY_USER"=>$USER->GetId()
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement()){ 
  $arFields = $ob->GetFields();
  $arFields['PROPERTIES'] = $ob->GetProperties();
  $totalCheck = count($_REQUEST['productoptions']);
  $propertyCount = 0;
  foreach ($_REQUEST['productoptions'] as $optionCode=>$options){
    foreach ($arFields['PROPERTIES']['OPTIONS']['VALUE'] as $propertyKey=>$value){
      if ($optionCode == $value){
        $porpertyDesctiption = $arFields['PROPERTIES']['OPTIONS']['DESCRIPTION'][$propertyKey];
        if ($options == $porpertyDesctiption){
          $propertyCount++;
        }
      }
    }
  }
  if($totalCheck == $propertyCount){
    $InIblock = true;
    $CurrentElementId = $arFields['ID'];
  }
}
if (!$InIblock){
  $el = new CIBlockElement;
  $arProps = array();
  foreach ($_SESSION['USER']['CART']['productoptions'] as $key=>$options){
    $arProps['OPTIONS'][] = array('VALUE'=>$key,'DESCRIPTION'=>$options);
  }
  $arProps['USER'] = $USER->GetId();
  $arProps['BASE_PRICE'] = $_SESSION['USER']['CART']['PRODUCT_BASE_PRICE'];
  $arProps['WEIGHT'] = $PritWeightPerItem;
  if ($CartQuantity){
    $arProps['QUANTITY'] = $CartQuantity;  
  }
  $arProps['ELEMENT_ID'] = $_SESSION['USER']['CART']['ELEMENT_ID'];
  $arProps['PRODUCT_ID'] = $_SESSION['USER']['CART']['PRODUCT_ID'];
  $arProps['AGENCY_PERCENT'] = $_SESSION['USER']['CART']['AGENCY_PERCENT'];
  $arProps['MADE'] = $_SESSION['USER']['CART']['productdelivery']['date'];
  $arProps['MADE_PRICE'] = $_SESSION['USER']['CART']['productdelivery']['price'];
  $arLoadProductArray = Array(
    "IBLOCK_ID" => 8,
    "PROPERTY_VALUES" => $arProps,
    "NAME" => $_SESSION['USER']['CART']['PRODUCT_NAME'],
    "ACTIVE" => "Y",
  );
    if($CurrentElementId = $el->Add($arLoadProductArray)){        
        $arCatalogFields = array(
            "ID" => $CurrentElementId, 
            "CAN_BUY_ZERO" => "Y",
            "QUANTITY" => "1",
            "WEIGHT"=> $PritWeightPerItem
        );
        if(CCatalogProduct::Add($arCatalogFields)) {
            //pr("Добавили параметры товара к элементу каталога ".$CurrentElementId);
        }
        else
            pr('Ошибка добавления параметров<br>');
        
        $arPriceFields = array(
            "PRODUCT_ID" => $CurrentElementId,
            "CATALOG_GROUP_ID" => 1,
            "PRICE" => $PrintPricePerItem,
            "CURRENCY" => "RUB",
        );
        $obPrice = new CPrice();
        $obPrice->Add($arPriceFields,true);
  }
  else {
    echo "Error: ".$el->LAST_ERROR;
  }
    
     
}

if (CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")){
    $arBasketItems = array();
    $dbBasketItems = CSaleBasket::GetList(
        array("NAME" => "ASC"),
        array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL"),
        false,
        false,
        array("ID", "PRODUCT_ID")
    );
    while ($arItems = $dbBasketItems->Fetch()){
        $arBasketItems[] = $arItems;
    }
    if (!$arBasketItems){
        $BASKET_ID = Add2BasketByProductID(
            $CurrentElementId, 
            $CartQuantity, 
            array(),
            array(
                array(
                    "NAME"=> "Тираж",
                    "CODE"=> "PRINT_RUN",
                    "VALUE" => $PrintRunValue,
                    "SORT" => "100"
                )
            ));
    }
    else {
        $inCart = reset($arBasketItems);
        if ($inCart['PRODUCT_ID'] != $CurrentElementId) {
            CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
            Add2BasketByProductID(
                $CurrentElementId, 
                $CartQuantity, 
                array(),
                array(
                    array(
                        "NAME"=> "Тираж",
                        "CODE"=> "PRINT_RUN",
                        "VALUE" => $PrintRunValue,
                        "SORT" => "100"
                    )
                )
            );
        }
    }
}
