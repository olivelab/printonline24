<?
$arSteps = array(
  array(
    'STEP'=>1,
    'NAME'=>'Корзина'
  ),
  array(
    'STEP'=>2,
    'NAME'=>'Регистрация /<br>Заполнение данных'
  ),
  array(
    'STEP'=>3,
    'NAME'=>'Параметры<br>доставки'
  ),
  array(
    'STEP'=>4,
    'NAME'=>'Заказ сформирован'
  )
);
?>
<div class="clearfix cart-steps">
  <? 
    $newStep = 0;
  foreach ($arSteps as $k=>$step):
    $k++;
    global $USER;
    if ($USER->IsAuthorized() && $step['STEP'] == 2) {
        continue;
    }
  ?>
  <div class="cart-step left<? if ($k == $STEP || !$STEP && $k == 1):?> selected<? endif?>">
  <? if ($USER->IsAuthorized() && $step['STEP'] > 2):?>
    <i><?=$step['STEP']-1;?></i>
  <? else:?>
    <i><?=$step['STEP'];?></i>
  <? endif;?>
    <span><?=$step['NAME'];?></span>
  </div>
  <? endforeach;?>
</div>