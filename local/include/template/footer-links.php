<div class="row">
  <div class="col-md-4">
    <h2>ОБЗОР ПРОДУКЦИИ</h2>
    <div class="row">
        <? $APPLICATION->IncludeComponent(
          "bitrix:menu", 
          "footer-menu", 
          array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "footer",
            "COMPONENT_TEMPLATE" => "footer-menu",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(
            ),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "footer",
            "USE_EXT" => "Y"
          ),
          false
        );?>
        
    </div>
  </div>
  <div class="col-md-4">
    <h2>Контакты</h2>
    <address><? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/address.php', Array(), Array("MODE"=> "html"));?></address>
  </div>
  <div class="col-md-4">
    <h2>Обслуживание клиентов</h2>
    <div class="footer-phone phone">
      <span>Мы на связи:</span>
      <? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/phone.php', Array(), Array("MODE"=> "html"));?>
      <? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/email.php', Array(), Array("MODE"=> "html"));?>
    </div>
    <div class="footer-work-time"><? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/work-time.php', Array(), Array("MODE"=> "html"));?></div>
  </div>
</div>