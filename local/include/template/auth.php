<?
global $USER;
if ($USER->IsAuthorized()):
    $arUser = BXExtra::UserInfo();
    
    if ($arUser['UF_TYPE'] == 2) {
        $headerUserName = $arUser['UF_LEGAL_VALUE']. ' "'. $arUser['WORK_COMPANY'].'"';
    }
    else {
        $headerUserName = $USER->GetFullName();
    }
?>
<a href="/personal/"><?=$headerUserName;?></a><span></span><a href="?logout=yes">Выход</a>
<? else:?>
<a href="<?=AJAX_PATH;?>auth.php" class="js-popUp fancybox.ajax">Вход</a><span></span><a href="/personal/register/">Регистрация</a>
<? endif;?>