<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */

$arComponentParameters = array(
    "ID" => array(
        "NAME" => "Produc ID",
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    "OPTIONS" => array(
        "NAME" => "Options",
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
