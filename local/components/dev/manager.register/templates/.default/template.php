<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	echo FormatHelper::Error(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<? endif?>
<? if ($arResult['MANAGER_REGISTER']):?>
    <? if ($arResult['VALUES']['USER_ID']):?>
    <script>
    $.ajax({
        url:AJAX_PATH + "orderOnUser.php",
        data:{USER_NEW_ID:<?=$arResult['VALUES']['USER_ID'];?>},
        success: function(data){
            $('#order-other-select').html(data);
            $.fancybox.close();
            initReloadFunction();
            submitForm('USER');
        }
    });
    </script>
    <? endif;?>
<? else:?>
<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="cart-detail" style="width:500px;">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
  <div class="cart-detail-personal">
    <input type="text" class="form-control js-mask_input" placeholder="Телефон *" data-inputmask="'mask': '+7 (999) 999-99-99'" name="REGISTER[PERSONAL_MOBILE]" value="<?=$arResult["VALUES"]["PERSONAL_MOBILE"]?>">
    <input type="text" class="form-control" placeholder="E-mail *" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>">
    <input type="password" class="form-control" placeholder="Пароль *" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"]?>">
    <input type="password" class="form-control" placeholder="Новый пароль еще раз *" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>">
  </div>
  <br><br>
  <div class="cart-detail-personal_selectors js-register_tabs" style="margin:0 1px;">
    <label><input type="radio" name="REGISTER[UF_TYPE]" value="1" <? if ($_REQUEST['REGISTER']['UF_TYPE']==1 || empty($_REQUEST['REGISTER']['UF_TYPE'])):?>checked<? endif;?>><span>Я - физическое лицо</span></label>
    <label><input type="radio" name="REGISTER[UF_TYPE]" value="2" <? if ($_REQUEST['REGISTER']['UF_TYPE']==2):?>checked<? endif;?>><span>Я - юридическое лицо</span></label>
  </div>
  <div class="cart-detail-personal_tab" style="margin:0 1px;">
    <div class="cart-detail-personal<? if ($_REQUEST['REGISTER']['UF_TYPE']==2):?> none<? endif;?>" id="tab1">
      <input type="text" class="form-control" placeholder="Имя *" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>">
      <input type="text" class="form-control" placeholder="Отчество" name="REGISTER[SECOND_NAME]" value="<?=$arResult["VALUES"]["SECOND_NAME"]?>">
      <input type="text" class="form-control" placeholder="Фамилия" name="REGISTER[LAST_NAME]" value="<?=$arResult["VALUES"]["LAST_NAME"]?>">
      <div class="form-control-date" onclick="BX.calendar({node: this, field: 'REGISTER[PERSONAL_BIRTHDAY]', bTime: false});">
        <input type="text" class="form-control" placeholder="Дата рождения" name="REGISTER[PERSONAL_BIRTHDAY]" value="<?=$arResult["VALUES"]["PERSONAL_BIRTHDAY"]?>">
        <i></i>
      </div>
    </div>
    
    <? 
    $arLegal = array();
    $UserField = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => 15));
    while( $UserFieldAr = $UserField->Fetch()){
       $arLegal[] = $UserFieldAr;
    }
    ?>
    <div class="cart-detail-personal<? if ($_REQUEST['REGISTER']['UF_TYPE']==1 || empty($_REQUEST['REGISTER']['UF_TYPE'])):?> none<? endif;?>" id="tab2">
      <div class="row">
        <div class="col-md-4">
            <div class="form-control-select">
            <?
            
            ?>
                <select class="chosen" name="REGISTER[UF_LEGAL]">
                <? foreach ($arLegal as $legal):?>
                    <option value="<?=$legal['ID'];?>" <? if ($arResult["VALUES"]['UF_LEGAL'] == $legal['ID']):?> selected<? endif;?>><?=$legal['VALUE'];?></option>                    
                <? endforeach;?>
                </select>
            </div>
        </div>
        <div class="col-md-8">
            <input type="text" class="form-control" placeholder="Название юридического лица *" name="REGISTER[WORK_COMPANY]" value="<?=$arResult["VALUES"]["WORK_COMPANY"]?>">
        </div>
      </div>
      <div class="row">
        <div class="col-md-6"><input type="text" class="form-control" placeholder="ИНН *" name="REGISTER[UF_INN]" value="<?=$_REQUEST['REGISTER']['UF_INN']?>"></div>
        <div class="col-md-6"><input type="text" class="form-control" placeholder="КПП *" name="REGISTER[UF_KPP]" value="<?=$_REQUEST['REGISTER']['UF_KPP']?>"></div>
      </div>
      <input type="text" class="form-control" placeholder="Юридический адрес *" name="REGISTER[WORK_STREET]" value="<?=$arResult["VALUES"]["WORK_STREET"]?>">
      <input type="text" class="form-control" placeholder="Телефон офиса" name="REGISTER[WORK_PHONE]" value="<?=$arResult["VALUES"]["WORK_PHONE"]?>">
    </div>
    
  </div>
  <div class="cart-detail-personal-requered">*Обязательные поля </div>
  <div class="cart-detail-personal-accept">
    <label class="checkbox"><input type="checkbox" checked name="REGISTER[SUBSCRIBE]"><i></i>Уведомлять о скидках/спецпредложений</label>
    <label class="checkbox"><input type="checkbox" checked name="REGISTER[ACCESS]"><i></i>Нажимая кнопку "Зарегистрироваться", я подтверждаю свою дееспособность, согласие на получение информации о статусе заказов и согласие на обработку и хранение персональных данных. <a href="/contacts/consent-to-the-processing-and-storage-of-personal-data/">Подробнее</a></label>
  </div>
  <div class="clearfix">
    <div class="right"><button class="btn btn-blue btn-w240" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>">Зарегистрироваться</button></div>
  </div> 
</form>
<script>
$(function(){
    $(document).on('change','.js-register_tabs input',function(){
        $('.cart-detail-personal_tab').find('.cart-detail-personal').hide();
        $('#tab'+$(this).val()).show();
    });
});
</script>
<? endif;?>