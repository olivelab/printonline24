<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponent $this
 */

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
	die();

global $USER_FIELD_MANAGER;

// apply default param values
$arDefaultValues = array(
	"SHOW_FIELDS" => array(),
	"REQUIRED_FIELDS" => array(),
	"AUTH" => "Y",
	"USE_BACKURL" => "Y",
	"SUCCESS_PAGE" => "",
);

foreach ($arDefaultValues as $key => $value)
{
	if (!is_set($arParams, $key))
		$arParams[$key] = $value;
}
if(!is_array($arParams["SHOW_FIELDS"]))
	$arParams["SHOW_FIELDS"] = array();
if(!is_array($arParams["REQUIRED_FIELDS"]))
	$arParams["REQUIRED_FIELDS"] = array();

// if user registration blocked - return auth form
if (COption::GetOptionString("main", "new_user_registration", "N") == "N")
	$APPLICATION->AuthForm(array());

$arResult["EMAIL_REQUIRED"] = (COption::GetOptionString("main", "new_user_email_required", "Y") <> "N");
$arResult["USE_EMAIL_CONFIRMATION"] = (COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && $arResult["EMAIL_REQUIRED"]? "Y" : "N");

// apply core fields to user defined
$arDefaultFields = array(
	"PERSONAL_MOBILE",
	"PASSWORD",
	"CONFIRM_PASSWORD",
);

if($arResult["EMAIL_REQUIRED"])
{
	$arDefaultFields[] = "EMAIL";
}

$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
if($def_group <> "")
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(explode(",", $def_group));
else
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(array());

$arResult["SHOW_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["SHOW_FIELDS"]));
$arResult["REQUIRED_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["REQUIRED_FIELDS"]));

$arResult["IND_SHOW_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["IND_FIELDS_SHOW"]));
$arResult["ENT_SHOW_FIELDS"] = array_unique(array_merge($arDefaultFields, $arParams["ENT_FIELDS_SHOW"]));

$arResult["IND_FIELDS_REQUIRED"] = array_unique(array_merge($arDefaultFields, $arParams["IND_FIELDS_REQUIRED"]));
$arResult["ENT_FIELDS_REQUIRED"] = array_unique(array_merge($arDefaultFields, $arParams["ENT_FIELDS_REQUIRED"]));

// use captcha?
$arResult["USE_CAPTCHA"] = COption::GetOptionString("main", "captcha_registration", "N") == "Y" ? "Y" : "N";

// start values
$arResult["VALUES"] = array();
$arResult["ERRORS"] = array();
$register_done = false;

// register user
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_REQUEST["register_submit_button"]) && !$USER->IsAuthorized())
{
    
    if ($_REQUEST['REGISTER']['UF_TYPE'] == 1){
        foreach ($arResult["IND_SHOW_FIELDS"] as $key){
            $arResult["VALUES"][$key] = $_REQUEST["REGISTER"][$key];
            if ($key == "PERSONAL_MOBILE"){                
                if(!preg_match('/(\+7|8) \(\d{3}\) (\d{3})-(\d{2})-(\d{2})/', $arResult["VALUES"][$key])) {
                    $arResult["VALUES"][$key] = "";
                    $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
                }
            }
            elseif ($key == "EMAIL") {
                if (!filter_var($arResult["VALUES"][$key], FILTER_VALIDATE_EMAIL)){
                    $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
                }
            }
            elseif (in_array($key, $arResult["IND_FIELDS_REQUIRED"]) && trim($arResult["VALUES"][$key]) == '') {
                $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
            }
        }
    }
    if ($_REQUEST['REGISTER']['UF_TYPE'] == 2){
        foreach ($arResult["ENT_SHOW_FIELDS"] as $key){
            $arResult["VALUES"][$key] = $_REQUEST["REGISTER"][$key];
            if ($key == "PERSONAL_MOBILE"){                
                if(!preg_match('/(\+7|8) \(\d{3}\) (\d{3})-(\d{2})-(\d{2})/', $arResult["VALUES"][$key])) {
                    $arResult["VALUES"][$key] = "";
                    $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
                }
            }
            elseif ($key == "UF_TYPE") {
            }
            elseif ($key == "EMAIL") {
                if (!filter_var($arResult["VALUES"][$key], FILTER_VALIDATE_EMAIL)){
                    $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
                }
            }
            elseif (in_array($key, $arResult["ENT_FIELDS_REQUIRED"]) && trim($arResult["VALUES"][$key]) == '') {
                $arResult["ERRORS"][$key] = GetMessage("REGISTER_FIELD_REQUIRED");
            }
        }
    }
    if ($_REQUEST['REGISTER']['ACCESS'] == ""){
        $arResult["ERRORS"]['ACCESS'] = "Необходимо принять условия";        
    }
	if(count($arResult["ERRORS"]) > 0)
	{
		if(COption::GetOptionString("main", "event_log_register_fail", "N") === "Y")
		{
			$arError = $arResult["ERRORS"];
			foreach($arError as $key => $error)
				if(intval($key) == 0 && $key !== 0) 
					$arError[$key] = str_replace("#FIELD_NAME#", '"'.$key.'"', $error);
			CEventLog::Log("SECURITY", "USER_REGISTER_FAIL", "main", false, implode("<br>", $arError));
		}
	}
	else // if there;s no any errors - create user
	{
		$bConfirmReq = (COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && $arResult["EMAIL_REQUIRED"]);
        $arResult['VALUES']['LOGIN'] = FormatHelper::makePhoneString($arResult['VALUES']['PERSONAL_MOBILE']);
		$arResult['VALUES']["CHECKWORD"] = md5(CMain::GetServerUniqID().uniqid());
		$arResult['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
		$arResult['VALUES']["ACTIVE"] = $bConfirmReq? "N": "Y";
		$arResult['VALUES']["CONFIRM_CODE"] = $bConfirmReq? randString(8): "";
		$arResult['VALUES']["LID"] = SITE_ID;

		$arResult['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
		$arResult['VALUES']["USER_HOST"] = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);
		
		if($arResult["VALUES"]["AUTO_TIME_ZONE"] <> "Y" && $arResult["VALUES"]["AUTO_TIME_ZONE"] <> "N")
			$arResult["VALUES"]["AUTO_TIME_ZONE"] = "";

		$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if($def_group != "")
			$arResult['VALUES']["GROUP_ID"] = explode(",", $def_group);

		$bOk = true;

		$events = GetModuleEvents("main", "OnBeforeUserRegister", true);
		foreach($events as $arEvent)
		{
			if(ExecuteModuleEventEx($arEvent, array(&$arResult['VALUES'])) === false)
			{
				if($err = $APPLICATION->GetException())
					$arResult['ERRORS'][] = $err->GetString();

				$bOk = false;
				break;
			}
		}
		$ID = 0;
		$user = new CUser();
		if ($bOk)
		{
			$ID = $user->Add($arResult["VALUES"]);
		}

		if (intval($ID) > 0)
		{
			$register_done = true;

			// authorize user
			if ($arParams["AUTH"] == "Y" && $arResult["VALUES"]["ACTIVE"] == "Y")
			{
				if (!$arAuthResult = $USER->Login($arResult["VALUES"]["LOGIN"], $arResult["VALUES"]["PASSWORD"]))
					$arResult["ERRORS"][] = $arAuthResult;
			}

			$arResult['VALUES']["USER_ID"] = $ID;

			$arEventFields = $arResult['VALUES'];
			unset($arEventFields["PASSWORD"]);
			unset($arEventFields["CONFIRM_PASSWORD"]);

			$event = new CEvent;
			$event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);
			if($bConfirmReq)
				$event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);
                
            
            if ($_REQUEST['REGISTER']['SUBSCRIBE']){
                if(CModule::IncludeModule('subscribe')) {
                    $arFields = Array(
                        "USER_ID" => $arResult['VALUES']["USER_ID"],
                        "FORMAT" => "html",
                        "EMAIL" => $arResult["VALUES"]["EMAIL"],
                        "ACTIVE" => "Y",
                        "RUB_ID" => "1"
                    );
                    $subscr = new CSubscription;
                    $subscrID = $subscr->Add($arFields);     
                }
            }
		}
		else
		{            
            if (strpos($user->LAST_ERROR, 'Пользователь с логином') !== false){
                $arResult["ERRORS"][] = 'Пользователь с телефоном "'.$arResult['VALUES']['PERSONAL_MOBILE'].'" уже существует.';
            }
            else {
			    $arResult["ERRORS"][] = $user->LAST_ERROR;
            }
		}

		if(count($arResult["ERRORS"]) <= 0)
		{
			if(COption::GetOptionString("main", "event_log_register", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER", "main", $ID);
		}
		else
		{
			if(COption::GetOptionString("main", "event_log_register_fail", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER_FAIL", "main", $ID, implode("<br>", $arResult["ERRORS"]));
		}

		$events = GetModuleEvents("main", "OnAfterUserRegister", true);
		foreach ($events as $arEvent)
			ExecuteModuleEventEx($arEvent, array(&$arResult['VALUES']));
	}
}

// if user is registered - redirect him to backurl or to success_page; currently added users too
if($register_done)
{
	if($arParams["USE_BACKURL"] == "Y" && $_REQUEST["backurl"] <> '')
		LocalRedirect($_REQUEST["backurl"]);
	elseif($arParams["SUCCESS_PAGE"] <> '')
		LocalRedirect($arParams["SUCCESS_PAGE"]);
}

$arResult["VALUES"] = htmlspecialcharsEx($arResult["VALUES"]);

// check backurl existance
$arResult["BACKURL"] = htmlspecialcharsbx($_REQUEST["backurl"]);


// get date format
if (in_array("PERSONAL_BIRTHDAY", $arResult["SHOW_FIELDS"])) 
	$arResult["DATE_FORMAT"] = CLang::GetDateFormat("SHORT");

// ********************* User properties ***************************************************
$arResult["USER_PROPERTIES"] = array("SHOW" => "N");
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER", 0, LANGUAGE_ID);

if (!empty($arResult["USER_PROPERTIES"]["DATA"]))
{
	$arResult["USER_PROPERTIES"]["SHOW"] = "Y";
	$arResult["bVarsFromForm"] = (count($arResult['ERRORS']) <= 0) ? false : true;
}
// ******************** /User properties ***************************************************

$arResult["SECURE_AUTH"] = false;
if(!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
{
	$sec = new CRsaSecurity();
	if(($arKeys = $sec->LoadKeys()))
	{
		$sec->SetKeys($arKeys);
		$sec->AddToForm('regform', array('REGISTER[PASSWORD]', 'REGISTER[CONFIRM_PASSWORD]'));
		$arResult["SECURE_AUTH"] = true;
	}
}

// all done
$this->IncludeComponentTemplate();
