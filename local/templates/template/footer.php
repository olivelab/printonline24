   <? if (!isStartPage() || GetPagePath() != "/"):?>
        <? if ($APPLICATION->GetDirProperty("view") != "menu"):?>
        </div>
        <? endif;?>
      </div>
    </div>
   </section>
   <? endif;?>
   <footer>
        <div class="container">
          <? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/footer-links.php', Array(), Array("MODE"=> "html"));?>
          <div class="copyr">© <?=date('Y');?> 
Оперативная: <a href="/">Полиграфия онлайн</a> в Москве
			  <br> <a href="/site-map/"> Карта сайта</a>
 </div>
        </div>
      </footer> 
  </div>
  <script type="text/javascript" src="<?=MEDIA_PATH;?>js/script.js"></script>
  <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "counters",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_TEMPLATE" => ""
    ),
    false
  );?>
</body>
</html>
