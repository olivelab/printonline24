<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE HTML>
<!--[if IE 7 ]> <html id="ie7" class="no-js"> <![endif]-->
<!--[if IE 8 ]> <html id="ie8" class="no-js"> <![endif]-->
<!--[if IE 9 ]> <html id="ie9" class="no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowMeta("robots")?>
		<?$APPLICATION->ShowMeta("keywords")?>
		<?$APPLICATION->ShowMeta("description")?>
		
		<link rel="shortcut icon" href="<?=MEDIA_PATH;?>favicon.ico" />
		<link href="<?=MEDIA_PATH;?>favicon.ico" rel="icon" type="image/x-icon" />
		<link href="<?=MEDIA_PATH;?>css/normalize.css" rel="stylesheet">
        <link href="<?=MEDIA_PATH;?>css/chosen.css" rel="stylesheet">
		<link href="<?=MEDIA_PATH;?>css/owl.carousel.css" rel="stylesheet">
		<link href="<?=MEDIA_PATH;?>css/jquery.fancybox.css" rel="stylesheet">		
		<link href="<?=MEDIA_PATH;?>css/animate.css" rel="stylesheet">
		<link href="<?=MEDIA_PATH;?>css/datepicker.css" rel="stylesheet">
		<link href="<?=MEDIA_PATH;?>css/base.css" rel="stylesheet">
		<link href="<?=MEDIA_PATH;?>css/style.css" rel="stylesheet">
		<script src="<?=MEDIA_PATH;?>js/library.js"></script>
		<script src="<?=MEDIA_PATH;?>js/lib/zeroclipboard/jquery.zeroclipboard.min.js"></script>        
		<script src="<?=MEDIA_PATH;?>js/lib/owl.carousel.min.js"></script>
        <script src="<?=MEDIA_PATH;?>js/lib/chosen.jquery.min.js"></script>
        <script src="<?=MEDIA_PATH;?>js/lib/jquery.inputmask.js"></script>        
        <script src="<?=MEDIA_PATH;?>js/lib/datepicker.js"></script>
        <script src="<?=MEDIA_PATH;?>js/lib/jquery.form.js"></script>
		<!--[if IE]>
		<link href="<?=MEDIA_PATH;?>css/base-ie.css" rel="stylesheet" type="text/css">
		<script src="<?=MEDIA_PATH;?>js/library-ie.js"></script>
		<![endif]-->
		
		<?$APPLICATION->ShowHeadStrings()?>
		<?$APPLICATION->ShowCSS();?>
		<?$APPLICATION->ShowHeadScripts()?>
		<? CJSCore::Init(array('ajax', 'window')) ;?>
		<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
			"AREA_FILE_SHOW" => "sect",
			"AREA_FILE_SUFFIX" => "header_analytics",
			"AREA_FILE_RECURSIVE" => "Y",
			"EDIT_TEMPLATE" => ""
			),
			false
		);?>
	</head>
	<body>
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div class="page">
      <header class="top">
        <div class="container clearfix">
          <div class="logo left"><a href="/" class="clearfix"><img src="<?=MEDIA_PATH;?>images/logo.png" alt="Оперативная полиграфия онлайн в Москве круглосуточно – печать блокнотов, буклетов, листовок, календарей, плакатов и другой продукции в любых объемах" title="Круглосуточная онлайн полиграфия в Москве – оперативно, качественно, надежно!" height="65" width="73" class="left"><span class="overflow block">Полиграфия<br><b>Онлайн</b></span></a></div>
          <div class="header-right right">
            <div class="header-auth right"><? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/auth.php', Array(), Array("MODE"=> "html"));?></div>
            <div class="clear"></div>
            <div class="header-bottom clearfix">
              <div class="header-phone left phone"><span>Мы на связи:</span><? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/phone.php', Array(), Array("MODE"=> "html"));?></div>
              <div class="header-cart left"><? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/cart.php', Array(), Array("MODE"=> "html"));?></div>
              <div class="header-fav left"><a href="#"></a></div>
            </div>
          <!--/Header right-->
          </div>
        </div>
      </header>
      <nav class="main-nav">
        <div class="container clearfix">
          <? $APPLICATION->IncludeComponent(
            "bitrix:menu", 
            "top-menu", 
            array(
              "ALLOW_MULTI_SELECT" => "N",
              "CHILD_MENU_TYPE" => "left",
              "COMPONENT_TEMPLATE" => "top-menu",
              "DELAY" => "N",
              "MAX_LEVEL" => "3",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "ROOT_MENU_TYPE" => "top",
              "USE_EXT" => "Y"
            ),
            false
          );?>
          
          <? $APPLICATION->IncludeFile(INCLUDE_PATH.'/template/search.php', Array(), Array("MODE"=> "html"));?>
        </div>
      </nav>
      <? if (!isStartPage() || GetPagePath() != "/"):?>
      <section class="content">
        <div class="container">
          <div class="row">
          <? if ($APPLICATION->GetDirProperty("view") == "menu"):?>
            <div class="col-md-3">
              <? $APPLICATION->IncludeComponent(
                "bitrix:menu", 
                "left-menu", 
                array(
                  "ALLOW_MULTI_SELECT" => "N",
                  "CHILD_MENU_TYPE" => "left",
                  "COMPONENT_TEMPLATE" => "left-menu",
                  "DELAY" => "N",
                  "MAX_LEVEL" => "1",
                  "MENU_CACHE_GET_VARS" => array(
                  ),
                  "MENU_CACHE_TIME" => "3600",
                  "MENU_CACHE_TYPE" => "N",
                  "MENU_CACHE_USE_GROUPS" => "Y",
                  "ROOT_MENU_TYPE" => "left",
                  "USE_EXT" => "Y"
                ),
                false
              );?>
            </div>
            <div class="col-md-9">
          <? else:?>
            <div class="col-md-12">            
         <? endif;?>
         
          <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
            "COMPONENT_TEMPLATE" => ".default",
                "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                "SITE_ID" => "-",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
            ),
            false
        );?>
          <h1><?=$APPLICATION->ShowTitle();?></h1>
      <? endif;?>

	