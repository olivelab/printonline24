<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<? /*if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;*/?>
<? 
$submited = false;
if ($arResult['arrVALUES']['WEB_FORM_ID']) $submited = true;?>
<?
if ($arResult["isFormNote"] != "Y")
{
?>
<div class="form-new">
<?=$arResult["FORM_HEADER"]?>
    <h2>Написать нам</h2>
<? if ($arResult['FORM_ERRORS']):
  foreach($arResult['FORM_ERRORS'] as $error){
    echo FormatHelper::Error($error);
  }
endif;?>    
<? 
  foreach ($arResult['QUESTIONS'] as $code=>$arQuestion):
  $formFieldInfo = reset($arQuestion['STRUCTURE']);
  $inputName = 'form_'.$formFieldInfo['FIELD_TYPE'].'_'.$formFieldInfo['ID'];
?>
  <label><?=$arQuestion['CAPTION'];?><? if ($arQuestion['REQUIRED']=="Y"):?>*<? endif;?></label>
  <? if ($formFieldInfo['FIELD_TYPE'] == "textarea"):?>
  <textarea 
    class="form-control <?=isset($arResult['FORM_ERRORS'][$code])?'pinput-require':''?>" 
    placeholder="" 
    rows="4" 
    name="<?=$inputName;?>"><?=$arResult['arrVALUES'][$inputName]?></textarea>
  <? else:?>
    <input 
      type="<?=$formFieldInfo['FIELD_TYPE'];?>" 
      name="<?=$inputName;?>" 
      class="form-control"
      value="<?=$arResult['arrVALUES'][$inputName]?>"
    >
  <? endif;?>  
<?
  endforeach;
?>    
<br>
    <div class="block-btn">
      <button type="submit" class="btn btn-blue btn-w200" name="web_form_submit" value="Отправить">Отправить</button>
    </div><br>
    <p><span class="star-required">*</span> Поле, обязательное для заполнения</p>
<?=$arResult["FORM_FOOTER"]?>
</div>
<?
} //endif (isFormNote)
else {
	?>
	<p class='success'><?=FormatHelper::Success('Спасибо! Ваша заявка принята!');?></p>

<?
}
?>