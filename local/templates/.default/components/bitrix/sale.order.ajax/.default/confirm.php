<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
    unset($_SESSION['USER']['CART']);
    /*
    $arResult['ORDER']['ID']
    */
    $OrderProp = array();
    $rsOrder = CSaleOrderPropsValue::GetList(
        array("SORT" => "ASC"),
        array("ORDER_ID" => $arResult["ORDER"]["ID"], "CODE"=>array("CITY", "DATE", "HOUSE", "USER_NEW_ID"))
    );
    while ($arOrderProps = $rsOrder->GetNext()){
        $OrderProp[$arOrderProps['CODE']] = $arOrderProps;
    }
    if ($OrderProp['USER_NEW_ID']['VALUE']) {
        $arNewUser = array(
            "USER_ID" => $OrderProp['USER_NEW_ID']['VALUE']
        );
        CSaleOrder::Update($arResult["ORDER"]["ID"], $arNewUser);
    }
    
	?>
    <? $APPLICATION->IncludeFile(INCLUDE_PATH.'/cart-steps.php', Array('STEP'=>4), Array("MODE"=> "html"));?>    
    <h1>Поздравляем!</h1>
    <div class="order-result_total">Вы сделали заказ № <?=$arResult["ORDER"]["ACCOUNT_NUMBER"];?> от <?=$arResult["ORDER"]["DATE_INSERT"];?> на сумму <?=number_format($arResult["ORDER"]["PRICE"],2,',',' ');?> руб.</div>
    <div class="order-result_delivery"><? 
    if ($arResult["ORDER"]["DELIVERY_ID"] == 15):?>Самовывоз из г.Железнодорожный 24 Апреля с 10.00 до 18.00 часов<? 
    else:
        $CurrentAddress = '';
        $res = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                '=ID' => $OrderProp['CITY']["VALUE"], 
                '=PARENTS.NAME.LANGUAGE_ID' => LANGUAGE_ID,
                '=PARENTS.TYPE.NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ),
            'select' => array(
                'I_NAME_RU' => 'PARENTS.NAME.NAME',
                'I_TYPE_CODE' => 'PARENTS.TYPE.CODE',
                'I_TYPE_NAME_RU' => 'PARENTS.TYPE.NAME.NAME'
            ),
            'order' => array(
                'PARENTS.DEPTH_LEVEL' => 'asc'
            )
        ));
        while($item = $res->fetch())
        {
            if ($item['I_TYPE_CODE'] == "CITY") $CurrentAddress = "г. ".$item['I_NAME_RU'];
            else $CurrentAddress .= ", ".$item['I_NAME_RU'];
        }
    ?>Доставка <?=$OrderProp['DATE']['VALUE'];?> с 10.00 до 18.00 часов, по адресу: <?=$CurrentAddress;?>, <?=$OrderProp['HOUSE']['VALUE'];?><?
    endif;?></div>
    <br>
    <div>Вы можете в любое время загрузить макет к заказу в <a href="/personal/?ID=<?=$arResult["ORDER"]["ACCOUNT_NUMBER"];?>">Личном кабинете</a></div>
    <div class="order-result_links">
    <? //pr($arResult["ORDER"]);?>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
        if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
        {

            if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
            {
                ?>
                <a href="<?=$arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"];?>" class="print-link" target="_blank">Распечатать счет</a>
                <?
                if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
                {
                    ?>
                <span></span>
                <a href="<?=$arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y";?>" class="download-link" target="_blank">Скачать счет</a>
                    <?
                }
            }
            else
            {
                if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
                {
                    try
                    {
                        include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                    }
                    catch(\Bitrix\Main\SystemException $e)
                    {
                        if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
                            $message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
                        else
                            $message = $e->getMessage();

                        echo '<span style="color:red;">'.$message.'</span>';
                    }
                }
            }
        }
	}
    ?>    
    </div>
<?    
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
