<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$CurrentUserType = $CurrentUser['UF_TYPE'];
if (!$CurrentUserType) $CurrentUserType = 1;
if ($CurrentUserType == 3) {
    $CurrentUserType = 2;
}
if ($NewUserType){
    $CurrentUserType = $NewUserType;
}
$UserType = $arResult["PERSON_TYPE"][$CurrentUserType];
?>
<input type="hidden" name="PERSON_TYPE" value="<?=$UserType["ID"]?>" id="PERSON_TYPE">
<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$UserType["ID"]?>" />
