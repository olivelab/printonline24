<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $CurrentUser)
	{
		if (!empty($arSource))
		{
				foreach ($arSource as $arProperties)
				{
					?>
						<?
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							<div class="bx_block r1x3 pt8">
								<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">
								<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<? if ($arProperties["CHECKED"]=="Y") echo " checked";?>>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
                            /*
                            FormatHelper::makePhoneString($CurrentUser['PERSONAL_MOBILE'])
                            */
                            /*if ($arProperties['CODE'] == "USER_NEW_ID") {
                                continue;
                            }*/
                            if (
                                $arProperties['CODE'] == "EMAIL" ||
                                $arProperties['CODE'] == "NAME" ||
                                $arProperties['CODE'] == "PHONE" ||
                                $arProperties['CODE'] == "INN" ||
                                $arProperties['CODE'] == "SURENAME" ||
                                $arProperties['CODE'] == "LASTNAME" ||
                                $arProperties['CODE'] == "PRINT_RUN" ||
                                $arProperties['CODE'] == "AGENCY_PERCENT" ||
                                $arProperties['CODE'] == "KPP" ||
                                $arProperties['CODE'] == "MANAGER_ID"||
                                $arProperties['CODE'] == "USER_NEW_ID"
                            ):
                                if ($arProperties['CODE'] == "PRINT_RUN") $arProperties['VALUE'] = floor($_SESSION['USER']['CART']['productoptions']['PRINT_RUN']);
                                if ($arProperties['CODE'] == "AGENCY_PERCENT") $arProperties['VALUE'] = $_SESSION['USER']['CART']['AGENCY_PERCENT'];
                                if ($CurrentUser['UF_TYPE'] == 2 && $arProperties['CODE'] == "NAME") $arProperties["VALUE"] = $CurrentUser["WORK_COMPANY"];
                                if ($arProperties['CODE'] == "INN")  $arProperties["VALUE"] = $CurrentUser['UF_INN'];
                                if ($arProperties['CODE'] == "KPP")  $arProperties["VALUE"] = $CurrentUser['UF_KPP'];
                                if ($arProperties['CODE'] == "SURENAME")  $arProperties["VALUE"] = $CurrentUser['SECOND_NAME'];
                                if ($arProperties['CODE'] == "LASTNAME")  $arProperties["VALUE"] = $CurrentUser['LAST_NAME'];
                                if ($arProperties['CODE'] == "USER_NEW_ID")  $arProperties["VALUE"] = $_REQUEST['USER_NEW_ID'];
                                if ($CurrentUser['MANAGER_ID']){
                                    if ($arProperties['CODE'] == "MANAGER_ID")  $arProperties["VALUE"] = $CurrentUser['MANAGER_ID'];
                                    if ($arProperties['CODE'] == "EMAIL")  $arProperties["VALUE"] = $CurrentUser['EMAIL'];
                                    if ($arProperties['CODE'] == "NAME" && $CurrentUser['UF_TYPE'] == 1)  $arProperties["VALUE"] = $CurrentUser['NAME'];
                                }
                                if ($arProperties['CODE'] == "PHONE")  {
                                    if ($CurrentUser['UF_TYPE'] == 2) {
                                        $arProperties["VALUE"] = $CurrentUser['WORK_PHONE']?$CurrentUser['WORK_PHONE']:$CurrentUser['PERSONAL_MOBILE'];
                                    }
                                    else {
                                        $arProperties["VALUE"] = $CurrentUser['PERSONAL_MOBILE']?$CurrentUser['PERSONAL_MOBILE']:$CurrentUser['PERSONAL_PHONE'];
                                    }
                                }
                                global $USER;
                            ?>
                            <input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" data-code="<?=$arProperties['CODE'];?>" value='<?=$arProperties["VALUE"]?>' id="<?=$arProperties["FIELD_NAME"]?>">
                            <? else:?>
                            <? if ($arProperties['CODE'] == "HOUSE"):?><div class="col-md-2"><? endif;?>
                            <input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" class="form-control" placeholder="<?=$arProperties['NAME'];?>">
                            <? if ($arProperties['CODE'] == "HOUSE"):?></div><? endif;?>
                            <?
                            endif;
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
							<div class="bx_block r3x1">
								<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?foreach($arProperties["VARIANTS"] as $arVariants):?>
										<option value="<?=$arVariants["VALUE"]?>"<?=$arVariants["SELECTED"] == "Y" ? " selected" : ''?>><?=$arVariants["NAME"]?></option>
									<?endforeach?>
								</select>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?foreach($arProperties["VARIANTS"] as $arVariants):?>
										<option value="<?=$arVariants["VALUE"]?>"<?=$arVariants["SELECTED"] == "Y" ? " selected" : ''?>><?=$arVariants["NAME"]?></option>
									<?endforeach?>
								</select>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<div class="bx_block r3x1">
								<textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?=$arProperties["VALUE"]?></textarea>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
                            $value = 0;
                            if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
                            {
                                foreach ($arProperties["VARIANTS"] as $arVariant)
                                {
                                    if ($arVariant["SELECTED"] == "Y")
                                    {
                                        $value = $arVariant["ID"];
                                        break;
                                    }
                                }
                            }

                            // here we can get '' or 'popup'
                            // map them, if needed
                            if(CSaleLocation::isLocationProMigrated())
                            {
                                $locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
                                $locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
                            }
                            ?>

                            <? if($locationTemplateP == 'steps'):?>
                                <input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" value="<?=($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0')?>" />
                            <? endif?>
                            <div class="col-md-10">
                            <? CSaleLocation::proxySaleAjaxLocationsComponent(array(
                                "AJAX_CALL" => "N",
                                "COUNTRY_INPUT_NAME" => "COUNTRY",
                                "REGION_INPUT_NAME" => "REGION",
                                "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                                "CITY_OUT_LOCATION" => "Y",
                                "LOCATION_VALUE" => $value,
                                "ORDER_PROPS_ID" => $arProperties["ID"],
                                "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                                "SIZE1" => $arProperties["SIZE1"],
                            ),
                                array(
                                    "ID" => $value,
                                    "CODE" => "",
                                    "SHOW_DEFAULT_LOCATIONS" => "Y",

                                    // function called on each location change caused by user or by program
                                    // it may be replaced with global component dispatch mechanism coming soon
                                    "JS_CALLBACK" => "submitFormProxy",

                                    // function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
                                    // it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
                                    "JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

                                    // an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
                                    // it may be replaced with global component dispatch mechanism coming soon
                                    "JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

                                    "DISABLE_KEYBOARD_INPUT" => "N",
                                    "PRECACHE_LAST_LEVEL" => "Y",
                                    "PRESELECT_TREE_TRUNK" => "Y",
                                    "SUPPRESS_ERRORS" => "Y"
                                ),
                                $locationTemplateP,
                                true,
                                'location-block-wrapper'
                            );
                            ?>
                            </div>
                            <?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r3x1">
								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<input
											type="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
									<?
									endforeach;
								}
								?>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
                            if ($arProperties['CODE'] == "MAKET") continue;
							?>
							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "DATE")
						{
                            $dateDelivery = $arProperties["VALUE"]?$arProperties["VALUE"]:$_SESSION['USER']['CART']['productdelivery']['date'];
							?>
                            <div class="col-md-5">
                            <br>
                                <h2><?=$arProperties['NAME'];?></h2>
                                <div>
                                    <input type="text" readonly id="datepicker" class="form-control form-datepicker" name="ORDER_PROP_<?=$arProperties["ID"];?>" value="<?=$dateDelivery;?>" style="border:1px solid #dadada !important" data-start="<?=$_SESSION['USER']['CART']['productdelivery']['date'];?>">                                    
                                </div>
                            </div>
							<?
						}
						?>

					<?if(CSaleLocation::isLocationProEnabled()):?>

					<?
					$propertyAttributes = array(
						'type' => $arProperties["TYPE"],
						'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
					);

					if(intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
						$propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

					if(intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
						$propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

					if($arProperties['IS_ZIP'] == 'Y')
						$propertyAttributes['isZip'] = true;
					?>

						<script>

							<?// add property info to have client-side control on it?>
							(window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
									'id' => intval($arProperties["ID"]),
									'attributes' => $propertyAttributes
								))?>);

						</script>
					<?endif?>

					<?
				}
				?>
			<?
		}
	}
}
?>