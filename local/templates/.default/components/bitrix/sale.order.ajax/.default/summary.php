<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>
<textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" placeholder="Комментарии к заказу" rows="3" class="form-control"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
<div class="cart-detail-personal-requered">*Обязательные поля </div>
<? if ($arResult["DELIVERY_PRICE"] != 0):?>
<div class="order-delivery_price text-right"><span>Стоимость доставки:<b><?=number_format($arResult["DELIVERY_PRICE"],2,',',' ')?> <span class="rur">a</span></b></span></div>
<? endif;?>
<table class="cart-table" width="100%">
<tbody><tr>
  <td class="text-left">
    <div class="cart-detail-weight">Общий вес: <b><?=$arResult["ORDER_WEIGHT_FORMATED"]?></b></div>
  </td>
  <td class="text-right">
    <div class="cart-detail-total_price">
      <div class="ib">Итого:</div>
      <div class="cart-detail_total ib"><?=number_format($arResult["ORDER_PRICE"]+$arResult["DELIVERY_PRICE"],2,',',' ');?> <span class="rur">a</span></div>
    </div>
  </td>
</tr>
</tbody>
</table>
<br><br>
