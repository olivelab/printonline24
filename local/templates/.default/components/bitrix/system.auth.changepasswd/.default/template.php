<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?
$result = $APPLICATION->arAuthResult;
if ($result['TYPE'] == "ERROR"){
    echo FormatHelper::Error(str_replace("�����", "�������", $result['MESSAGE']));
}
elseif ($result['TYPE'] == "OK") {
    echo FormatHelper::Success($result["MESSAGE"]);
}
?>
<div class="bx-auth">

<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
	<?if (strlen($arResult["BACKURL"]) > 0): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">

    <div><b><?=GetMessage("AUTH_LOGIN")?></b></div>
    <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="form-control" />
    <div><b><?=GetMessage("AUTH_CHECKWORD")?></b></div>
    <input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="form-control" />
    <div><b><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></b></div>
    <input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="form-control" autocomplete="off" />
    <div><b><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></b></div>
    <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="form-control" autocomplete="off" />
    <br>
    <button class="btn btn-blue btn-w200" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>"><?=GetMessage("AUTH_CHANGE")?></button>
<br><br>
<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><?=GetMessage("AUTH_REQ")?></p>
<p>
<a href="/local/include/ajax/auth.php" class="js-popUp fancybox.ajax"><?=GetMessage("AUTH_AUTH")?></a>
</p>

</form>

<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
</div>