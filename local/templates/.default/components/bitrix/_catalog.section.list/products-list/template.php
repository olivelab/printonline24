<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="index-products">
  <div class="container">
    <h2 class="text-center">Что мы делаем для Вас?</h2>
    <div class="index-product-list clearfix">        
<?
foreach ($arResult['SECTIONS'] as &$arSection):
  $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
  $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
  //pr($arSection['UF_BG']);
?>
<a href="<?=$arSection['SECTION_PAGE_URL'];?>" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" style="background-color:#<?=$arSection['UF_BG'];?>"><i><img src="<?=$arSection['PICTURE']['SRC'];?>"></i><span><?=$arSection['NAME'];?></span></a>
<? endforeach; ?>
    </div>
  </div>
</div>