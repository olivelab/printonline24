<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$result = $APPLICATION->arAuthResult;
if ($result['TYPE'] == "ERROR"){
    echo FormatHelper::Error(str_replace("Логин", "Телефон", $result['MESSAGE']));
}
elseif ($result['TYPE'] == "OK") {
    echo FormatHelper::Success($result["MESSAGE"]);
}
?>
<form method="post" action="<?=$arResult["AUTH_URL"]?>">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">
	<p>
	<?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
	</p>
<input type="text" class="form-control" name="USER_LOGIN" maxlength="50" placeholder="Номер телефона в формате 7**********" value="<?=$arResult["LAST_LOGIN"]?>" size="17">
<p class="text-center">
<button class="btn btn-blue btn-w200" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>"><?=GetMessage("AUTH_SEND")?></button>
<br><br>
<a href="<?=AJAX_PATH?>auth.php" class="js-popUp fancybox.ajax"><?=GetMessage("AUTH_AUTH")?></a>
</p> 
</form>
