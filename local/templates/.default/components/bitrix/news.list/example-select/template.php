<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$toreplace = array('\'', ' ', '"');
$InputName = 'productoptions['.$arParams['OPTION']['name'].']';
$arOptions = array();
$arKeyOptions = array();
function string_sanitize($s) {
    $result = preg_replace("/[^a-zA-Zа-яА-Я0-9]+/", "", html_entity_decode($s, ENT_QUOTES));
    return $result;
}
foreach($arResult["ITEMS"] as $arItem){
    $key = string_sanitize($arItem['NAME']);
    $arOptions[$arItem['ID']] = array(
        'ID' => $arItem['ID'],
        'NAME' => string_sanitize($arItem['NAME'])
    );
    $arKeyOptions[$arItem['ID']] = string_sanitize($arItem['NAME']);
}
if ($arParams['SELECTED']){
    foreach ($arParams['OPTION']['options'] as $option){
        if ($option['value'] == $arParams['SELECTED']){
            $selectedOptions = $option;
            break;
        }
    }
}
else {
    $selectedOptions = reset($arParams['OPTION']['options']);
}
?>
<div class="example-select">
    <div class="example-current js-open-example">
        <?=trim($selectedOptions['title']);?>
    </div>
    <div class="example-list">
        <div class="clearfix">
            <div class="left example-list_links">
            <? 
            foreach ($arParams['OPTION']['options'] as $option):
                $key = array_search(string_sanitize($option['title']), $arKeyOptions);
                $InIblock = $arOptions[$key];
            ?>
            <a href="#" data-value="<?=$option['value'];?>" data-name="<?=$InputName;?>" data-id="<?=$InIblock['ID'];?>" class="js-show-example-pic<? if (addslashes($option['title']) == addslashes($selectedOptions['title'])):?> selected<? endif;?>"><?=$option['title'];?></a>
            <? endforeach;?>
            </div>
            <div class="left example-list_pics">
            <? foreach($arResult["ITEMS"] as $arItem):?>
            <? if ($arItem['PREVIEW_PICTURE']):?>
            <span id="pic<?=$arItem['ID'];?>"<? if (string_sanitize($arItem['NAME']) != string_sanitize($selectedOptions['title'])):?> class="none"<? endif;?>><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="<?=$arItem['NAME'];?>" class="img-responsive"></span>
            <? endif;?>
            <? endforeach;?>
            </div>
        </div>
    </div>
    <input name="<?=$InputName;?>" type="hidden" value="<?=$selectedOptions['value'];?>" class="js-product_detail-update">
</div>