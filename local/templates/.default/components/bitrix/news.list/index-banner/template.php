<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index-banner shadow-after">
  <div class="container">
    <div class="banner-carousel">
    <? foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
      <div class="banner-carousel-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div style="line-height:280px;" class="text-center">
        <? if ($arItem['PROPERTIES']['URL']['VALUE']):?><a href="<?=$arItem['PROPERTIES']['URL']['VALUE'];?>"><? endif;?>
          <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="" class="img-responsive">        
        <? if ($arItem['PROPERTIES']['URL']['VALUE']):?></a><? endif;?>
        </div>
      </div>
    <? endforeach;?>
    </div>
  </div>
<script>
$(function(){
  $('.banner-carousel').owlCarousel({
    loop:true,
    merge:true,
    nav:true,
    dots:false,
    autoplay:false,
    autoplayTimeout: 4000,
    autoHeight:true,
    items:1,
    autoplayHoverPause:true,
  })
});
</script>
</div>