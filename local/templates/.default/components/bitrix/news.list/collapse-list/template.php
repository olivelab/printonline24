<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script>
$(window).on('ready',function(){
    if (location.hash){
        var target = $(location.hash);
    console.log(target.offset().top)
        setTimeout(function(){
            $('body,html').animate({scrollTop:target.offset().top});
        },1000);
        console.log($('a[href="'+location.hash+'"]').text());
        $('a[href="'+location.hash+'"]').click();
    }
});
</script>
<div class="collapse-list">
  <? foreach($arResult["ITEMS"] as $arItem):?>
  <?
  $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
  $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
  ?>
  <div class="collapse-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    <a href="#<?=$arItem['ID'];?>_collapse" class="js-collapse_link"><?=$arItem['NAME'];?></a>
    <div class="collapse-detail none" id="<?=$arItem['ID'];?>_collapse"><?=$arItem['PREVIEW_TEXT'];?></div>
  </div>
  <? endforeach;?>
</div>