<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="col-md-6">
  <ul class="no-list">
<?
$previousLevel = 0;
$k = 0;
foreach($arResult as $arItem):
?>
  <li<? if ($arItem['SELECTED']):?> class="selected"<? endif;?>>
    <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
  </li>
<?
$k++;
if (ceil(count($arResult)/2) == $k):?>
  </ul>
</div>
<div class="col-md-6">
  <ul class="no-list">
<?
endif;
endforeach?>
  </ul>
</div>
<?endif?>