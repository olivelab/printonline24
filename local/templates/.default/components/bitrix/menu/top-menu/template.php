<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<menu class="main-menu no-list left-list left">
<?
$previousLevel = 0;
foreach($arResult as $arItem):
?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>
	<?if ($arItem["IS_PARENT"]):?>
    <li<? if ($arItem['SELECTED'] || $arItem["CHILD_SELECTED"]):?> class="selected"<? endif;?>>
      <a href="<?=$arItem["LINK"]?>"><? if ($arItem['PARAMS']['IMG']):?><i style="background:#<?=$arItem['PARAMS']['BG'];?>"><img src="<?=$arItem['PARAMS']['IMG'];?>" alt=""></i><? endif;?><?=$arItem["TEXT"]?><span></span></a>
      <ul class="main-menu-sub no-list">
	<?else:?>
		<li<? if ($arItem['SELECTED']):?> class="selected"<? endif;?>>
      <a href="<?=$arItem["LINK"]?>"><? if ($arItem['PARAMS']['IMG']):?><i style="background:#<?=$arItem['PARAMS']['BG'];?>"><img src="<?=$arItem['PARAMS']['IMG'];?>" alt=""></i><? endif;?><?=$arItem["TEXT"]?></a>
    </li>
	<?endif?>
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</menu>
<?endif?>