<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$IsAgancy = false;  
$IsManager = false;   
$arUser = BXExtra::UserInfo();
if (in_array(AGENCY_GROUP,$arUser['GROUP'])){
    $IsAgancy = true;
}
if (in_array(MANAGER_GROUP,$arUser['GROUP'])){
    $IsManager = true;
}
?>

<div class="bx-auth-profile">
<?
if ($arResult["strProfileError"]){
    echo FormatHelper::Error($arResult["strProfileError"]);
}
if ($arResult['DATA_SAVED'] == 'Y'){
    echo FormatHelper::Success(GetMessage('PROFILE_DATA_SAVED'));
}
    CJSCore::Init(array('date'));
?>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<!---->
<? if ($arResult['arUser']['UF_TYPE'] == 1):?>
    <label><?=GetMessage('NAME')?> *</label>
    <input type="text" class="form-control" name="NAME" value="<?=$arResult["arUser"]["NAME"]?>">
    <input type="text" class="form-control" placeholder="<?=GetMessage('SECOND_NAME')?>" name="SECOND_NAME" value="<?=$arResult["arUser"]["SECOND_NAME"]?>">
    <input type="text" class="form-control" placeholder="<?=GetMessage('LAST_NAME')?>" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>">
    <div class="form-control-date" onclick="BX.calendar({node: this, field: 'PERSONAL_BIRTHDAY', bTime: false});">
        <input type="text" class="form-control" placeholder="Дата рождения" name="PERSONAL_BIRTHDAY" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"];?>">
        <i></i>
    </div>
<? elseif($arResult['arUser']['UF_TYPE'] == 2):
    /*For nat*/
    ?>
    <? 
    $arLegal = array();
    $UserField = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => 15));
    while( $UserFieldAr = $UserField->Fetch()){
       $arLegal[] = $UserFieldAr;
    }
    ?>
    <label><?=GetMessage('USER_COMPANY')?> *</label>
    <div class="row">
        <div class="col-md-3">
            <div class="form-control-select">
                <select class="chosen" name="UF_LEGAL">
                <? foreach ($arLegal as $legal):?>
                    <option value="<?=$legal['ID'];?>" <? if ($arResult['arUser']['UF_LEGAL'] == $legal['ID']):?> selected<? endif;?>><?=$legal['VALUE'];?></option>                    
                <? endforeach;?>
                </select>
            </div>
        </div>
        <div class="col-md-9">
            <input type="text" class="form-control" name="WORK_COMPANY" value="<?=$arResult["arUser"]["WORK_COMPANY"]?>">
        </div>
    </div>
    <? 
    foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
        if ($FIELD_NAME == "UF_MARGIN") continue;
    ?>
    <label><?=$arUserField['EDIT_FORM_LABEL']?> *</label>
    <input type="text" class="form-control" placeholder="" name="<?=$arUserField['FIELD_NAME']?>" value="<?=$arUserField['VALUE']?>">
    <? endforeach;?>
    <label>Юридический адрес *</label>
    <input type="text" class="form-control" name="WORK_STREET" value="<?=$arResult["arUser"]["WORK_STREET"]?>">
    <? if ($IsAgancy):?>
        <label>Телефон офиса</label>
        <input type="text" class="form-control" name="WORK_PHONE" value="<?=$arResult["arUser"]["WORK_PHONE"]?>">
        <label><?=GetMessage('USER_WWW')?></label>
        <input type="text" class="form-control" name="WORK_WWW" value="<?=$arResult["arUser"]["WORK_WWW"]?>">
        <label><?=GetMessage('USER_LOGO')?></label>
        <?=$arResult["arUser"]["WORK_LOGO_INPUT"]?>
        <?
        if (strlen($arResult["arUser"]["WORK_LOGO"])>0)
        {
        ?>
            <br /><div><?=$arResult["arUser"]["WORK_LOGO_HTML"]?></div>
        <?
        }
        ?>
    <? endif;?>
    <? if ($IsManager || $IsAgancy):?>
        <br>
        <label>Наценка</label>
        <div class="form-control-select">
            <select class="chosen" name="UF_MARGIN">
            <? for ($i=0;$i<=100;$i+=5):?>
            <option value="<?=$i;?>"<? if ($arResult["USER_PROPERTIES"]["DATA"]["UF_MARGIN"]['VALUE'] == $i):?> selected<? endif;?>><?=$i;?>%</option>
            <? endfor;?>
            </select>
        </div> 
    <? endif;?>
<? endif;?>

<!---->
<?
//$arResult['arUser']['UF_TYPE']
?>
<hr>
<label>Номер телефона / Логин</label>
<div class="form-control">+7 <?=FormatHelper::makePhoneFormat(!is_numeric($arResult["arUser"]["LOGIN"])?$arResult["arUser"]['PERSONAL_MOBILE']:$arResult["arUser"]["LOGIN"]);?></div>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<label><?=GetMessage('EMAIL')?> *</label>
<input type="text" class="form-control" name="EMAIL" value="<? echo $arResult["arUser"]["EMAIL"]?>">
<label><?=GetMessage('NEW_PASSWORD_REQ')?> *</label>
<input type="password" class="form-control" name="NEW_PASSWORD" value="" autocomplete="off">
<label><?=GetMessage('NEW_PASSWORD_CONFIRM')?> *</label>
<input type="password" class="form-control" name="NEW_PASSWORD_CONFIRM" value="" autocomplete="off">
<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<?if($arResult["IS_ADMIN"]):?>
<textarea cols="30" rows="5" class="form-control" name="ADMIN_NOTES" placeholder="<?=GetMessage("USER_ADMIN_NOTES")?>"><?=$arResult["arUser"]["ADMIN_NOTES"]?></textarea>
<?endif;?>

<div class="clearfix">
    <div class="right"><button class="btn btn-blue btn-w240" name="save" value="Регистрация">Сохранить</button></div>
</div>
</form>
</div>