<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
 echo FormatHelper::Error($arResult['ERROR_MESSAGE']['MESSAGE']);
	?>
<?
}
?>

<?
if($arResult["FORM_TYPE"] == "login"):
$_SESSION['NOT_AUTH'] = "Y";
if (mb_strlen($arResult["USER_LOGIN"])==11 && empty($_POST)){
    $arResult["USER_LOGIN"] = substr($arResult["USER_LOGIN"],1,14);
} 
?>
<form name="system_auth_form<?=$arResult["RND"]?>" method="post" action="<?=$arResult["AUTH_URL"]?>">
	<? if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
  <? endif?>
	<input type="hidden" name="reload" value="Y">
	<input type="hidden" name="ajax" value="Y">
<? foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<? endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
  <input type="text" class="form-control js-mask_input" name="USER_LOGIN" maxlength="50" placeholder="Телефон" data-inputmask="'mask': '+7 (999) 999-99-99'" value="<?=$arResult["USER_LOGIN"]?>" size="17">
  <input type="password" class="form-control" placeholder="Пароль" name="USER_PASSWORD" maxlength="50" size="17">
  <div class="clearfix form-links">
    <div class="left"><label class="checkbox"><input type="checkbox" checked type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y"><i></i>Запомнить меня</label></div>
    <div class="right"><a href="<?=AJAX_PATH;?>forget.php" class="js-popUp fancybox.ajax">Забыли пароль?</a></div>
  </div>
  <button class="btn btn-blue btn-w200" type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">Войти</button>
</form>
<? else:
if ($_SESSION['NOT_AUTH'] == "Y"){
    $arUser = BXExtra::UserInfo();
    
    if ($arUser['UF_TYPE'] == 2) {
        $headerUserName = $arUser['UF_LEGAL_VALUE']. ' "'. $arUser['WORK_COMPANY'].'"';
    }
    else {
        $headerUserName = $USER->GetFullName();
    }
	unset($_SESSION['NOT_AUTH']);?>
	<script>
	window.location.reload();
	</script>
<?
}
?>
<div class="top_auth left">
	<div class="top_auth-name"><a href="/personal/">Здравствуйте, <?=$headerUserName?></a>, <a href="?logout=yes&reload=Y">выход</a></div>
</div>
<?endif?>