<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="product-detail">
  <div class="product-detail_gallery clearfix row">
    <div class="col-md-9">
      <div class="product-detail_gallery-detail js-singleCarousel in-carousel-arrows">
      <? 
      foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $photId):
      $middle = CFile::ResizeImageGet($photId, array('width'=>500, 'height'=>313), BX_RESIZE_IMAGE_EXACT, true);
      ?>
      <a href="<?=CFile::GetPath($photId);?>" class="js-popUp" rel="middle_photos">
        <img src="<?=$middle['src'];?>" alt="" height="<?=$middle['height'];?>" width="<?=$middle['width'];?>">
      </a>
      <? endforeach;?>
      </div>
    </div>
    <div class="col-md-3">
      <div class="product-detail_gallery-list owl-dots">
        <? 
        $c = 1;
        foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $photId):
        $small = CFile::ResizeImageGet($photId, array('width'=>150, 'height'=>91), BX_RESIZE_IMAGE_EXACT, true);
        ?>
        <div class="owl-dot" rel="small_photos">
          <img src="<?=$small['src'];?>" alt="" height="<?=$small['height'];?>" width="<?=$small['width'];?>">
        </div>
        <? 
        $c++;
        if ($c>3) break;
        endforeach;?>
      </div>
    </div>
  </div>
  <div class="product-detail_text">
    <?=$arResult['DETAIL_TEXT'];?>
  </div>
  <div id="product-result">
  <?
  if ($arParams['OPTIONS_URL']){
    $linkOptions = $arParams['OPTIONS_URL'];
  }
  ?>
    <? if ($arResult['PROPERTIES']['PRODUCT_ID']['VALUE']):?>
    <? $APPLICATION->IncludeFile(
          AJAX_PATH.'/detail_product.php', 
          array(
            'ELEMENT_ID'=>$arResult['ID'],
            'PRODUCT_ID' => $arResult['PROPERTIES']['PRODUCT_ID']['VALUE'],
            'linkOptions' => $linkOptions,
            'agentPercent' => $arParams['AGENT_PERCENT']
          ), 
          array(
            "SHOW_BORDER"=> false
          )
        );
    ?>
    <? endif;?>
  </div>
  <? if ($arResult['PROPERTIES']['BOTTOM_TEXT']['~VALUE']['TEXT']):?>
  <div class="product-form_detail">
    <?=$arResult['PROPERTIES']['BOTTOM_TEXT']['~VALUE']['TEXT'];?>    
  </div>
  <? endif;?>
</div>