<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section-item-list">
<?
foreach ($arResult['ITEMS'] as $key => $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);
?>
  <a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="clearfix" id="<?=$strMainID;?>">
    <i class="left"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></i>
    <span class="overflow">
      <span class="section-item-list-name"><?=$arItem['NAME'];?></span>
      <span class="section-item-list-text"><?=$arItem['PREVIEW_TEXT'];?></span>
    </span>
  </a>
<? endforeach;?>
</div>