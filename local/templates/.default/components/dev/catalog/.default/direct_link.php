<?
if ($arResult['VARIABLES']['UNIQCODE']){
  $arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*");
  $arFilter = Array("IBLOCK_ID"=>4, "ACTIVE"=>"Y", "PROPERTY_LINK"=>$arResult['VARIABLES']['UNIQCODE']);
  $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
  $arElementOptions = array();
  $AgentPercent = false;
  while($ob = $res->GetNextElement()){ 
    $arFields = $ob->GetFields();
    $arFields['PROPERTIES'] = $ob->GetProperties();
    $arResult["VARIABLES"]["ELEMENT_ID"] = $arFields['PROPERTIES']['ELEMENT_ID']['VALUE'];
    if ($arFields['PROPERTIES']['AGENCY_PERCENT']){
        $AgentPercent = $arFields['PROPERTIES']['AGENCY_PERCENT']['VALUE'];
    }
    foreach ($arFields['PROPERTIES']['OPTIONS']['VALUE'] as $k=>$value){
      $arElementOptions[$value] = $arFields['PROPERTIES']['OPTIONS']['DESCRIPTION'][$k];
    }
  }
}
include($_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/element.php');
?>