var AJAX_PATH = '/local/include/ajax/',
    INCLUDE_PATH = '/local/include/',
    MEDIA_PATH = '/local/media/';
function getLuma(color) {
	var rgba = color.substring(4, color.length - 1).split(',');
	var r = rgba[0];
	var g = rgba[1];
	var b = rgba[2];
	var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b;
	return luma;
};
function getBackground(item) {
	var color = item.css("background-color");
	var alpha = parseFloat(color.split(',')[3], 10);
	if ((isNaN(alpha) || alpha > 0.8) && color !== 'transparent') {
		return color;
	}
	if (item.is("body")) {
		return false;
	} else {
		return this.getBackground(item.parent());
	}
};
function initReloadFunction(){
    $('.js-mask_input').inputmask({
        clearIncomplete: true
    });    
    $('.chosen').chosen({
        "width":"100%", 
        disable_search: true,
        no_results_text: "Совпадений не найдено",
        placeholder_text_single: "Нет вариантов"
    });
    
    $('select.chosen-search').chosen({
        width:"100%", 
        disable_search:false,
        disable_search_threshold:0,
        no_results_text: "Совпадений не найдено",
        placeholder_text_single: "Нет вариантов"
    });
    
	//if ($('input[placeholder]').length || $('textarea[placeholder]').length)$('input[placeholder], textarea[placeholder]').placeholder();
}
$(function(){//DOM READEY
    $('.js-upload').ajaxForm({
        delegation: true,
        replaceTarget:true,
        beforeSerialize: function(formData) {
            formData.closest('.bx_my_order').addClass('loading');
        },
        uploadProgress: function(event, position, total, percentComplete) {
        },
        success: function(responseText,statusText,xhr,form) {
            $(form).closest('.bx_my_order').removeClass('loading');
            $(form).closest('.order_template').html(responseText);
        }
    });
     
    $('.order_template').on('change','input[type="file"]',function(){
        $(this).closest('.js-upload').submit();
    });
	if (window.PIE) {
      $('.pie').each(function() {
          PIE.attach(this);
      });
    }
    $('.js-mask_input').inputmask({
        clearIncomplete: true
    });
	initReloadFunction();
    $('.fancybox').fancybox({});
    $('.js-popUp').fancybox({
		wrapCSS: 'page',
		padding : [46,25,25,25],
		height:"auto",
        minHeight:40,
		fitToView: false,
		afterShow: function(current, previous) {
			initReloadFunction();
		}
	});
	$(document).on('submit','form',function(e){
		$(this).find('input').each(function(index, element) {
			if ($(this).hasClass('placeholder')) $(this).val("");
		});
		$(this).find('textarea').each(function(index, element) {
			if ($(this).hasClass('placeholder')) $(this).val("");
		});
	});
	if (typeof BX === "function") {
		BX.addCustomEvent('onAjaxSuccess', function() {
			initReloadFunction();
            $.fancybox.update();
            $('.js-mask_input').inputmask({
                clearIncomplete: true
              });
		});
	}
  
	$(document).on('click','.ink-reaction, .btn', function(e) {
		var bound = $(this).get(0).getBoundingClientRect();
		var x = e.clientX - bound.left;
		var y = e.clientY - bound.top;
		var color = getBackground($(this));
		var inverse = (getLuma(color) > 183) ? ' inverse' : '';
		var ink = $('<div class="ink' + inverse + '"></div>');
		var btnOffset = $(this).offset();
		var xPos = e.pageX - btnOffset.left;
		var yPos = e.pageY - btnOffset.top;
		ink.css({top: yPos,left: xPos}).appendTo($(this));
		window.setTimeout(function() {
			ink.remove();
		}, 1500);
	});
    $('.js-leftMenu').on('click',function(e){
        e.preventDefault();
        $('.left-menu ul').slideUp();
        $(this).next('ul').slideToggle();
        $(this).closest('li').toggleClass('selected');
        $(this).toggleClass('selected');
    });
    $('.js-singleCarousel').owlCarousel({
        loop:true,
        nav:true,
        autoplay:false,
        autoHeight:true,
        items:1,
        autoplayHoverPause:true,
        dotsContainer:'.product-detail_gallery-list'
    });
    $(document).on('change','.js-product_detail-update',function(){
        var form = $(this).closest('.product_form'),
            data = form.serialize();
        $('#product-result').addClass('load-info');
        $.ajax({
            url:AJAX_PATH+'detail_product.php',
            data:data,
            success: function(result){
                $('#product-result').html(result);
                initReloadFunction();
                $('#product-result').removeClass('load-info');
            }
        });
    });
    $(document).on('click','.js-cart_empty',function(e){
        e.preventDefault();
        $.ajax({
            url:AJAX_PATH+'cart.php',
            dataType:"json",
            data:{delete:true},
                success: function(result){
                window.location.reload();
                $('.header-cart').html(result.basket);
            }
        });
    });
    $(document).on('click','.js-replaceItem',function(e){
        e.preventDefault();
        var form = $('.product_form'),
            data = form.serialize();
        $.ajax({
            url:AJAX_PATH+'cart.php',
            dataType:"json",
            data:data,
            success: function(result){
                $('.header-cart').html(result.basket);
                $.fancybox.close();
            }
        });
    });
  $(document).on('submit','.js-add_cart',function(e){
    e.preventDefault();
    var form = $(this).closest('.product_form'),
        data = form.serialize();
    
    if ($('.header-cart i').length){
        $.fancybox(['<h2 style="white-space:nowrap">У Вас уже есть товар в корзине!</h2> <div>Обратите внимание, что по техническим и организационным причинам в корзине может быть только один вид продукции.<br>Если Вы хотите заказать несколько видов продукции, пожалуйста, оформите отдельные заказы на каждый вид продукции.</div><br><div class="text-center"><a href="#" class="js-replaceItem btn btn-green">Заменить товар в корзине</a></div>'],{
          wrapCSS: 'page',
          padding : [40,25,25,25],
          height:"auto",
          minHeight:40,
          fitToView: false,
        });
    }
    else {
    /*$('#product-result').addClass('load-info');*/
        $.ajax({
          url:AJAX_PATH+'cart.php',
          dataType:"json",
          data:data,
          success: function(result){
            $.fancybox(['<h2 style="white-space:nowrap">Продукция добавлена в корзину</h2><br><div class="text-center"><a href="/personal/cart/" class="btn btn-green">Оформить</a></div>'],{
              wrapCSS: 'page',
              padding : [40,25,25,25],
              height:"auto",
              minHeight:40,
              fitToView: false,
            });
            $('.header-cart').html(result.basket);
          }
        });
    }
  });
    $(document).on('click','.js-short_link',function(e){
        e.preventDefault();
        var link = $(this).data('link'),
            data = $('.product_form').serialize();
        $.ajax({
            url:AJAX_PATH+'short_links.php',
            data:data,
            success: function(result){            
                $('.short_link-url').html(result);
            }
        });
    });
  
    $(document).on('click','.js-make_pdf',function(e){
        e.preventDefault();
        var data = $('.product_form').serialize();
        $('#product-result').addClass('load-info');
        $.ajax({
            url:AJAX_PATH+'make_pdf-store.php',
            data:data,
            dataType:"json",
            success: function(result){            
                if (result.result){
                    window.location = AJAX_PATH+'make_pdf.php';
                    $('#product-result').removeClass('load-info');
                }
                else {
                    console.log(result);
                }
            }
        });
    });
    $(document).on('click','.js-collapse_link',function(e){
        e.preventDefault();
        $($(this).attr('href')).slideToggle();
    });
    $("body").on("copy", ".js-copyText", function( e) {
        e.preventDefault();
        e.clipboardData.clearData();
        e.clipboardData.setData("text/plain", $(this).data("copy"));
    });
    $("body").on('click', '.js-open-example', function(e){
        e.preventDefault();
        $(this).closest('.example-select').toggleClass('selected');
    });
    $("body").on({
        'click':function(e){
            e.preventDefault();
            var InputName = $(this).data('name'),
                InputValue = $(this).data('value');
            $('input[name="'+InputName+'"]').val(InputValue).trigger('change');
            $(this).closest('.example-select').removeClass('selected');
            
        },
        'mouseenter':function(e){
            var id = $(this).data('id');
            $('.example-list_links a.selected').addClass('was-selected').removeClass('selected');
            $('.example-list_pics span').addClass('none');
            $('.example-list_pics #pic'+id).removeClass('none');
        },
        'mouseleave':function(e){
            var container = $(this).closest('.example-list_links');
                link = $(container).find('.was-selected'),
                id = link.data('id');
            link.addClass('selected').removeClass('was-selected');
            $('.example-list_pics span').addClass('none');
            $('.example-list_pics #pic'+id).removeClass('none');
        }
    },'.js-show-example-pic');
});