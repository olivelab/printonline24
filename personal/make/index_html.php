<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Адрес доставки");
?>

<? $APPLICATION->IncludeFile(INCLUDE_PATH.'/cart-steps.php', Array('STEP'=>3), Array("MODE"=> "html"));?>

<form class="order-delivery">
  <div class="row">
    <div class="col-md-5">
      <input type="text" class="form-control" placeholder="Город *">
    </div>
    <div class="col-md-5">
      <input type="text" class="form-control" placeholder="Улица *">
    </div>
    <div class="col-md-2">
      <input type="text" class="form-control" placeholder="Дом/строение">
    </div>
  </div>
  <br><br>
  <h2>Дата доставки</h2>
  <div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-3">
      <h3 class="order-night">Ночь / Утро</h3>
      <div class="order-labels">
        <label class="radio"><input type="radio" name="night" checked><i></i>с 00:00 до 02:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 02:00 до 04:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 04:00 до 06:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 06:00 до 08:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 08:00 до 10:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 10:00 до 12:00</label>
      </div>
    </div>
    <div class="col-md-3">
      <h3 class="order-day">День</h3>
      <div class="order-labels">
        <label class="radio"><input type="radio" name="night" checked><i></i>с 12:00 до 14:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 14:00 до 16:00</label>
        <label class="radio"><input type="radio" name="night"><i></i>с 16:00 до 18:00</label>
      </div>
    </div>
  </div>
  <br>
  <textarea placeholder="Комментарии к заказу" rows="3" class="form-control"></textarea>
  <div class="cart-detail-personal-requered">*Обязательные поля </div>
  <div class="order-delivery_price text-right"><span>Стоимость доставки:<b>6000,00 <span class="rur">a</span></b></span></div>
  <table class="cart-table" width="100%">
    <tbody><tr>
      <td class="text-left">
        <div class="cart-detail-weight">Общий вес: <b>8,01 кг</b></div>
      </td>
      <td class="text-right">
        <div class="cart-detail-total_price">
          <div class="ib">Итого:<small>(с учетом доставки и вкл. 18% НДС)</small></div>
          <div class="cart-detail_total ib">14 337,87 <span class="rur">a</span></div>
        </div>
      </td>
    </tr>
  </tbody>
  </table>
  <br><br>
  <div class="clearfix">
    <div class="right"><button class="btn btn-blue btn-w240">Далее</button></div>
  </div> 
</form>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>