<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
<? 
if ($_SESSION['USER']['CART']):
    $CartData = $_SESSION['USER']['CART'];
    $PRODUCT_ID = $CartData['PRODUCT_ID'];
    $requestOptions = $CartData['productoptions'];
    $product = new ProductDetail;
    $arResult = $product->getResult($PRODUCT_ID,$requestOptions);
?>
<? $APPLICATION->IncludeFile(INCLUDE_PATH.'/cart-steps.php', Array(), Array("MODE"=> "html"));?>

<div class="cart-detail">
  <h2><?=$arResult['prod_name'];?><small>Добавлено в корзину: <?=$CartData['DATE'];?></small></h2>
  <table class="cart-table" width="100%">
  <? 
  foreach ($arResult['base-option'] as $option):
  $selected = $requestOptions[$option['name']];
  ?>
    <tr>
      <td width="320"><?=$option['title'];?></td>
      <? foreach ($option['options'] as $k=>$select):?>
      <? if ($selected == $select['value']):?>
        <td><?=$select['title'];?></td>
      <? endif;?>
      <? endforeach;?>      
    </tr>
  <? 
  endforeach;?>
  </table>
  <div class="cart-detail-price text-right"><?=number_format($CartData['PRODUCT_BASE_PRICE'],2,',',' ');?> <span class="rur">c</span></div>
  <br><br>
  <table class="cart-table" width="100%">
    <? 
    foreach ($arResult['ext-options'] as $option):
    $selected = $requestOptions[$option['name']];
    ?>
      <tr>
        <td width="320"><?=$option['title'];?></td>
        <? foreach ($option['options'] as $k=>$select):?>
        <? if ($selected == $select['value']):?>
          <td><?=$select['title'];?></td>
        <? endif;?>
        <? endforeach;?>  
        <td class="text-right"><?=number_format($option['price'],2,',',' ');?> <span class="rur">a</span></td>    
      </tr>
    <? 
    endforeach;?>
    <? if ($CartData['productdelivery']):?>
    <tr>
        <td width="320">Срок изготовления</td>
        <td><?=$CartData['productdelivery']['date'];?></td>
        <td class="text-right"><?=number_format($CartData['productdelivery']['price']?$CartData['productdelivery']['price']:0,2,',',' ');?> <span class="rur">a</span></td>
    <? endif;?>
  </table>   
  <br><br>
  <table class="cart-table" width="100%">
    <tr>
      <td class="text-left">
        <div class="cart-detail-weight">Общий вес: <b><?=number_format($arResult['total-weight'],2,',',' ');?> кг</b></div>
      </td>
      <td class="text-right">
        <div class="cart-detail-total_price">
          <div class="ib">Итого:</div>
          <div class="cart-detail_total ib"><?=number_format($CartData['PRODUCT_TOTAL_PRICE'],2,',',' ');?> <span class="rur">a</span></div>
        </div>
      </td>
    </tr>
  </table>
  <br><br>
  <div class="clearfix">
    <div class="left"><a href="#" class="btn btn-border-blue btn-w240 js-cart_empty">Очистить корзину</a></div>
    <div class="right"><a href="/personal/order/" class="btn btn-blue btn-w240">Оформить заказ</a></div>
  </div> 
</div>
<? else:?>
 <h2 class="text-center">Ваша корзина пуста</h2>
<? endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>