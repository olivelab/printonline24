<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
CJSCore::Init(array('date')) ;
RedirectToRegister();
global $USER;
if ($USER->IsAuthorized()){
    LocalRedirect('/personal/make/');
}

?>

<? $APPLICATION->IncludeFile(INCLUDE_PATH.'/cart-steps.php', Array('STEP'=>2), Array("MODE"=> "html"));?>
<?$APPLICATION->IncludeComponent(
	"dev:main.register",
	"",
	Array(
		"AUTH" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE", "WORK_COMPANY"),
		"SUCCESS_PAGE" => "",
        "IND_FIELDS_SHOW" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE","UF_TYPE"),
        "ENT_FIELDS_SHOW" => array("PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET","WORK_PHONE"),
        "IND_FIELDS_REQUIRED" => array("PERSONAL_MOBILE", "EMAIL", "NAME","UF_TYPE"),
        "ENT_FIELDS_REQUIRED" => array( "PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET"),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "getSpecial"
	)
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>