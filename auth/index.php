<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if($_GET['forgot_password']=='yes'):
$APPLICATION->SetTitle("Запрос пароля на восстановление");
elseif($_GET['change_password']=='yes'):
$APPLICATION->SetTitle("Востановление пароля");
elseif($_GET['register']=='yes'):
$APPLICATION->SetTitle("Регистрация");
else:
$APPLICATION->SetTitle("Авторизация");
endif;
global $USER;
if ($USER->IsAuthorized())
{
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
  LocalRedirect($backurl);
}
?>
<div class="content container clearfix" style="width:400px">
<?global $USER;
if ($USER->IsAuthorized())
{?> 
<p>Вы зарегистрированы и успешно авторизовались.</p>
 
<p><a href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>
 <?}?>


 <?global $USER;
if (!$USER->IsAuthorized())
{?>


<?if($_GET['forgot_password']=='yes'):?>
 <?$APPLICATION->IncludeComponent(
   "bitrix:system.auth.forgotpasswd","popup",false
);?>

 <?elseif($_GET['change_password']=='yes'):?>
 <?$APPLICATION->IncludeComponent(
   "bitrix:system.auth.changepasswd","",false
);?>

 <?elseif($_GET['register']=='yes'):
CJSCore::Init(array('date')) ;?>
 <?$APPLICATION->IncludeComponent(
	"dev:main.register",
	"",
	Array(
		"AUTH" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE", "WORK_COMPANY"),
		"SUCCESS_PAGE" => "",
        "IND_FIELDS_SHOW" => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_MOBILE","UF_TYPE"),
        "ENT_FIELDS_SHOW" => array("PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET","WORK_PHONE"),
        "IND_FIELDS_REQUIRED" => array("PERSONAL_MOBILE", "EMAIL", "NAME","UF_TYPE"),
        "ENT_FIELDS_REQUIRED" => array( "PERSONAL_MOBILE", "EMAIL", "WORK_COMPANY","UF_TYPE", "UF_INN", "UF_KPP","WORK_STREET"),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
        "AJAX_MODE" => "Y",
        "REDIRECT"=>"/personal/make/",
        "AJAX_OPTION_JUMP" => "Y",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "getSpecial"
	)
);?>

 <?else:?>
 <?$APPLICATION->IncludeComponent(
   "dev:system.auth.form",
   "popup",
   Array(
      "REGISTER_URL" => SITE_DIR."auth/",
      "PROFILE_URL" => SITE_DIR."/personal/register//",
      "SHOW_ERRORS" => "Y"
   )
);?>
<?endif?> <?}?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>