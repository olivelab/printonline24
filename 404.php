<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("view", "single");
$APPLICATION->SetTitle("404 Страница не найдена");

?>
<p>Приносим свои извинения, но данная страница не существует или была удалена. Чтобы продолжить работу с сайтом, воспользуйтесь одним из предложенных вариантов:</p>
<ul>
    <li>Перейдите на <a href="/">главную страницу</a>;</li>
    <li>Перейдите на <a href="/site-map/">карту сайта</a>;</li>
    <li>Используйте главное меню нашего сайта или строку поиска для того, чтобы найти интересующую информацию или продукцию.</li>
</ul>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>